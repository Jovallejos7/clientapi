<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

         view()->composer('layout.newMain', function ($view) {
            // DEFINE VARIABLES
            $userData = $_SESSION["SessionAPI"]['user'];
            $activation = "";
            $warning = "";
            $ms = "";
            if(isset($_SESSION["SessionAPI"]["activation"])){
                $activation = $_SESSION["SessionAPI"]["activation"];
                $ms = "Click en tu nombre de usuario -> información";
            }
            if(isset($_SESSION["SessionAPI"]["warning"])){
                $warning = $_SESSION["SessionAPI"]["warning"];
            }

            $with = array_merge([
                'userData' => $userData,
                'activation' => $activation,
                'warning' => $warning,
                'ms' => $ms,
            ], $view->getData());
            $view->with($with);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
