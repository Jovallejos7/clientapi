<?php

namespace App\Http\Controllers\View;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Goutte\Client as Goutte;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as RQ;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

class ExtractorsViewController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $objet=[];

    public function extractors (){
        
        return view('extractors.extractors', ['error' => 'none']);
    }

    //EXTRACTORES COMPARTIDOS POR LOS USUARIOS
    public function loadAllExtractors(){
        $client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('GET',$this->api_url.'/api/extractors', $header);
            $response =  $client->send($rq);
            $extractors = json_decode($response->getBody(), true);
            $finalData['data']=$extractors['data']['extractors'];
            return $finalData;
        }catch (\Exception $ex){
            return "Se produjo un error";
        }
    }

    //EJECUTAR EXTRACTOR
    public function executeExtractor(request $request){
        $data['extractorID']=$request->all()['id'];
        $url=$request->all()['url'];
        $client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('POST',$this->api_url.'/api/extractors/executeExtractor', $header, json_encode($data));
            $response =  $client->send($rq);
            $result = json_decode($response->getBody(), true);
            $extractorName = $result['data']['extractorName'];
            $extractedData = $result['data']['extractedData'];

            return view('extractors.executeExtractor', ['extractorName' => $extractorName, 'extractedData' => $extractedData, 'url' => $url]);
        }catch (\Exception $ex){
            return view('simpleErrors.error', ['error' => json_encode(json_decode($ex->getResponse()->getBody(), true)['errorsContent']), 'code' => $ex->getCode()]);
        }
    }

    //publico
    public function extractorsPublic (){
        
        return view('extractors.publicExtractor', ['error' => 'none']);
    }

    
    public function loadAllExtractorsPublic(){
        $client = new Client();
        $header = ['x-access-token'=>"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY3RpdmVkIjp0cnVlLCJlbWFpbCI6ImpvcmdlLnZhbGxlam9zLnByYWRvQGdtYWlsLmNvbSIsImlkIjoiYzE4ODg5NjMtNGViYS00OTYyLWJjZjgtZjlmMWIxZWNiNjA3IiwibG9naW4iOiJqb3JnZUFyaWVsNyIsInJlZ2lzdHJhdGlvbkRhdGUiOiIyMDE4LTExLTE1VDIzOjE3OjMwLjQ1M1oiLCJpYXQiOjE1NTQzNDYzNDR9.8V1J5UUWI7crwKaF_oheNKW7G72s_kXFmQJI-i870dY", 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('GET',$this->api_url.'/api/extractors', $header);
            $response =  $client->send($rq);
            $extractors = json_decode($response->getBody(), true);
            $finalData['data']=$extractors['data']['extractors'];
            return $finalData;
        }catch (\Exception $ex){
            return "Se produjo un error";
        }
    }

    //EJECUTAR EXTRACTOR
    public function executeExtractorPublic(request $request){
        $data['extractorID']=$request->all()['id'];
        $url=$request->all()['url'];
        $client = new Client();
        $header = ['x-access-token'=>"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY3RpdmVkIjp0cnVlLCJlbWFpbCI6ImpvcmdlLnZhbGxlam9zLnByYWRvQGdtYWlsLmNvbSIsImlkIjoiYzE4ODg5NjMtNGViYS00OTYyLWJjZjgtZjlmMWIxZWNiNjA3IiwibG9naW4iOiJqb3JnZUFyaWVsNyIsInJlZ2lzdHJhdGlvbkRhdGUiOiIyMDE4LTExLTE1VDIzOjE3OjMwLjQ1M1oiLCJpYXQiOjE1NTQzNDYzNDR9.8V1J5UUWI7crwKaF_oheNKW7G72s_kXFmQJI-i870dY", 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('POST',$this->api_url.'/api/extractors/executeExtractor', $header, json_encode($data));
            $response =  $client->send($rq);
            $result = json_decode($response->getBody(), true);
            $extractorName = $result['data']['extractorName'];
            $extractedData = $result['data']['extractedData'];

            return view('extractors.executeExtractorPublic', ['extractorName' => $extractorName, 'extractedData' => $extractedData, 'url' => $url]);
         }catch (\Exception $ex){
            return view('simpleErrors.errorPublic', ['error' => json_encode(json_decode($ex->getResponse()->getBody(), true)['errorsContent']), 'code' => $ex->getCode()]);
        }
    }
    //fin publico

     public function myExtractors (){

        return view('extractors.myExtractors', ['error' => 'none']);
    }

     public function twitter (){
        
        return view('extractors.twitterExtractors', ['error' => 'none', 'query' => '']);
    }

    //EJECUTAR EXTRACTOR DE TWITTER
    public function executeTwitterExtractor($querySearch){
        $pos = strpos($querySearch, "hashtag:");
        if ($pos !== false) {
             $querySearch = str_replace("hashtag:", "#", $querySearch);
        } 
    
        $data['querySearch']=$querySearch;
        $client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('POST',$this->api_url.'/api/extractors/twitter', $header, json_encode($data));
            $response =  $client->send($rq);
            $result = json_decode($response->getBody(), true);
            $tweets['data'] = $result['data']['_TweetsExtracted'];
            return $tweets;

        }catch (\Exception $ex){
            return $ex;
        }
    }

    public function executeTwitterExtractorSearchMain(Request $request){
        return view('extractors.twitterExtractors', ['error' => 'none', 'query' => $request->all()['search']]);
    }

    //QUERY DE TWITTER VACIA
    public function executeTwitterExtractorNull(){
        $data['data']=[];
        return $data;
    }

    //BUSCAR EXTRACTOR POR ID
    public function findExtractorById($id){
        $client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('GET',$this->api_url.'api/extractors/id/'.$id, $header);
            $response =  $client->send($rq);
            $extractors = json_decode($response->getBody(), true);
            $finalData['data']=$extractors['data']['extractors'];
            return $finalData;
        }catch (\Exception $ex){
            return "Se produjo un error";
        }
    }

    //BUSCAR EXTRACTOR POR NOMBRE
    Public function findExtractorByName($name){
        $client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('GET',$this->api_url.'api/extractors/name/'.$name, $header);
            $response =  $client->send($rq);
            $extractors = json_decode($response->getBody(), true);
            $finalData['data']=$extractors['data']['extractors'];
            return $finalData;
        }catch (\Exception $ex){
            return "Se produjo un error";
        }
    }

    //BUSCAR EXTRACTOR POR PAGINA WEB
    Public function findExtractorByUrl($url){
        $client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('GET',$this->api_url.'api/extractors/url/'.$url, $header);
            $response =  $client->send($rq);
            $extractors = json_decode($response->getBody(), true);
            $finalData['data']=$extractors['data']['extractors'];
            return $finalData;
        }catch (\Exception $ex){
            return "Se produjo un error";
        }
    }

    //PROBAR EXTRACTOR
    Public function TestExtractor(request $request){
        
        $data=$request->all();
        $extractor['testExtractorData']['name']= $data['name'];
        $extractor['testExtractorData']['url']= $data['url'];
        $extractor['testExtractorData']['nodes'] = [];

        for ($i=1; $i < 11 ; $i++) { 
            if(isset($data['name-'.$i])){

                $node['name']=$data['name-'.$i];
                $node['container']=$data['container-'.$i];
                $node['elements'] = [];

                for ($j=1; $j < 11 ; $j++) { 
                    if(isset($data['tag-'.$j.'-'.$i])){
                        $element['tag'] = $data['tag-'.$j.'-'.$i];
                        $element['selector'] = $data['selector-'.$j.'-'.$i];
                        $node['elements'][] = $element;
                    }
                    
                }
                $extractor['testExtractorData']['nodes'][] = $node;
            }
            
        }

//return ['true' => 1, 'data' => $extractor];
        $client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('POST',$this->api_url.'/api/extractors/testExtractor', $header, json_encode($extractor));
            $response =  $client->send($rq);
            $result = json_decode($response->getBody(), true);

            return ['true' => 1, 'data' => $result];

        }catch (ClientException $ex){
            switch ($ex->getCode()) {
                case 404:
                    return view('simpleErrors.404');
                    break;
                case 500:
                    return view('simpleErrors.404');
                    break;
            }
            //Se captura el error en la vista
            return ['true' => 1, 'data' => json_decode((string)$ex->getResponse()->getBody()->getContents(),true)];
            //Obtener json del error
        }
    }

    //EJECUTAR NODO
    Public function executeNode(request $request){
        $client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('POST',$this->api_url.'api/extractors/executeNode', $header, json_encode($data->all()));
            $response =  $client->send($rq);
            $result = json_decode($response->getBody(), true);
            return $result;
        }catch (\Exception $ex){
            return "Se produjo un error";
        }
    }

    //PROBAR NODO
    Public function testNode(request $request){
        $client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('POST',$this->api_url.'api/extractors/testNode', $header, json_encode($data->all()));
            $response =  $client->send($rq);
            $result = json_decode($response->getBody(), true);
            return $result;
        }catch (\Exception $ex){
            return "Se produjo un error";
        }
    }


    public function getNodes(request $Request){

        $client = new Goutte();
        $guzzleClient  = new Client(array(
            'timeout' => 600,
        ));
        $client->setClient($guzzleClient );
        $crawler = $client->request('GET', $Request->all()['url']);
        $this->newFunction2($crawler->filter('body')->children());
        //$clases = array_values(array_unique($this->objet));
        $clases=[];
        //$array = array('pedro', 'juan', 'paco', 'pedro' , 'juan', 'juan');
        $repeated = array_filter(array_count_values($this->objet), function($count) {
            return $count > 1;
        });
        foreach ($repeated as $key => $value) {
            if($value > 2 && $value <= 10){
                $clases[]=$key;
            }
        }

        return ['true'=> 1, 'data' => $clases];
    }

    public function newFunction2($crawler){

      
        $crawler->each(function ($node, $i) {
            if($node->nodeName()!="script" && $node->nodeName()!="footer"  && $node->nodeName()!="header"){

                if($node->parents()->extract('class')[0]!="" && $node->extract('class')[0]!=""){
                    $newKey=".".str_replace(" ",".",$node->parents()->extract('class')[0]);
                    $newKey=str_replace("..",".",$newKey);

                    $children=".".str_replace(" ",".",$node->extract('class')[0]);
                    $children=str_replace("..",".",$children);
                  
                    $pos = strpos($node->extract('_text')[0], "\n");

                    // Nótese el uso de ===. Puesto que == simple no funcionará como se espera
                    // porque la posición de 'a' está en el 1° (primer) caracter.
                    if ($pos !== false) {
                        $this->objet[]=$newKey." ".$children;
                        
                    } else {
                       
                    }

                    
                }

                $this->newFunction2($node->children());
                //Devolver elemntos repetidos
            }
            //$i+=1;
            //$this->newFunction($node->children());
        });
        // dd($objet);
        return true;
    }

        public function getElements(request $Request){

        $client = new Goutte();
        $guzzleClient  = new Client(array(
            'timeout' => 600,
        ));
        $client->setClient($guzzleClient );
        $crawler = $client->request('GET', $Request->all()['url']);
        $this->newFunction3($crawler->filter($Request->all()['nodo'])->children());
        $clases = array_values(array_unique($this->objet));
        //dd($clases);
        return ['true'=> 1, 'data' => $clases];
    }

    public function newFunction3($crawler){
      
        $crawler->each(function ($node, $i) {
            if($node->nodeName()!="script" && $node->nodeName()!="footer"  && $node->nodeName()!="header"){

                if($node->parents()->extract('class')[0]!="" && $node->extract('class')[0]!=""){
                    $newKey=".".str_replace(" ",".",$node->parents()->extract('class')[0]);
                    $newKey=str_replace("..",".",$newKey);

                    $children=".".str_replace(" ",".",$node->extract('class')[0]);
                    $children=str_replace("..",".",$children);
                   
                    //$pos = strpos($node->extract('_text')[0], "\n");

                    //si la clase es vacia obtener ELEMENTO EJEMPLO div > a > p
                    //if ($pos === false) {
                        $this->objet[]=$newKey." ".$children;
                        $this->objet[]=$children;
                        $this->objet[]=$newKey;
                    // } else {
                       
                    // }
                }

                $this->newFunction3($node->children());
                
            }
            //$i+=1;
            //$this->newFunction($node->children());
        });
        // dd($objet);
        return true;
    }




    //TEST TEST TEST TEST

    /**
     *
     */
    public function optenerdatos (){

        $client = new Goutte();
        $guzzleClient  = new Client(array(
            'timeout' => 600,
        ));
        $client->setClient($guzzleClient );
        $crawler = $client->request('GET', 'https://www.emol.com/');
       // $output = $crawler->filter('')->each(function ($node) {
          //dump($node->filter('')->extract('class'));
        //});array_filter($values, "strlen");
        //dd($crawler);
        $newCrawler = $crawler->nextAll();
        //dd($newCrawler);
        //$aer=$this->recorrerNodos($crawler);
//dump($aer);
        //CRAWLER INICIAL
        $output = $crawler->children();
        
       // $output = $crawler->filter('')->children()->each(function ($node) {
          //dump($node->extract('class'));
        //});

        //CLASES DEL CRAWLER
        //$class=$this->obtenerClases($crawler->filter('body')->children());
        /*$newKey="";
        foreach ($output as $key => $classes) {
            //dump($classes);
            $newKey=".".str_replace(" ",".",$classes);
            $newKey=str_replace("..",".",$newKey);
        
           
        }*/
        $nodeValues = $this->newFunction($crawler->filter('body')->children());
       // dd($class);
        //$MyClasses= $this->hijos($class, $crawler);
        dd($nodeValues);
        /*foreach ($class as $key => $value) {
            $output2 = $crawler->filter($key)->children();
            $class2=$this->obtenerClases($output2);
            dump($class2);
            # code...
        }*/
        
       //dump($class2);
        //$crawler->filter('div > div > a')->each(function ($node) {
          //  dump($node->text()."\n");
        //});
        
        //return $crawler;
    }

    public function newFunction($crawler){
      
        $crawler->each(function ($node, $i) {
            if($node->nodeName()!="script"){
                dump($node->extract('class'));
                $this->newFunction($node->children());
            }
            //$i+=1;
            //$this->newFunction($node->children());
        });
    }

    public function obtenerClases($crawler){
        $classChildren=[];
        $newKey = "";
        $output = array_filter($crawler->extract('class' ));
        //dump($output);
        if(!empty($output)){
            foreach ($output as $key => $classes) {
                //dump($classes);
                $newKey=".".str_replace(" ",".",$classes);
                $newKey=str_replace("..",".",$newKey);

                $outputCraw = $crawler->filter($newKey)->children();
                $classArray=$this->obtenerClases($outputCraw);

                if(empty($classArray)){
                    //dump("ESTA ES LA CLAVE: ".$newKey);
                    $outputCraw = $crawler->children();
                    $classArray=$this->obtenerClases($outputCraw);

                    $classChildren[$newKey]=$classArray;
                }
                else{
                    $classChildren[$newKey]=$classArray;
                }
                
            }

        }else {

           foreach ($crawler as $domElement) {
                //dump($domElement->nodeName);
                $outputCraw = $crawler->children();
                $classArray=$this->obtenerClases($outputCraw);
                if(empty($classArray)){
                    $classChildren[$domElement->nodeName]=$crawler->extract('_text');
                }
                else {
                    $classChildren[$domElement->nodeName]=$classArray;
                }
                
            }

            //$output = array_filter($crawler->extract('text'));
            //return $output;
        }
        
        return $classChildren;
    }

public function recorrerNodos($crawler){
     $classChildren=[];
     foreach ($crawler as $domElement) {
                //dump($domElement->nodeName);
                $outputCraw = $crawler->filter($domElement->nodeName)->children();
                $classChildren[$domElement->nodeName]=$this->recorrerNodos($outputCraw);
                
            }
return $classChildren;
}



    public function hijos ($class, $crawler){
        $MyClasses=[];

        foreach ($class as $key => $classesitas) {

            $class2=$class;
            
            //d($class2);
            //dump($key);
            if($crawler->filter($key)->children()->count()>0){
                $output = array_filter($crawler->filter($key)->children()->extract('class'), "strlen");
                $aux=[];
                foreach ($output as $classes) {
                    $newKey=".".str_replace(" ",".",$classes);
                    $newKey=str_replace("..",".",$newKey);

                    $aux[$newKey]=[];
                    dump("Buscando clases...");
                    $MyClasses= $this->hijos($aux, $crawler);
                    $class2[$key]=$MyClasses;
                   
                   //$MyClasses= $this->hijos($aux, $crawler);
                }
            }
            //dd($MyClasses);
        }
        return $class2;
        
    }

    public function sinonimosExtractorView (){
        return view('extractors.sinonimos');

    }

    public function sinonimosExtractor (Request $palabras){

            
        $cadena = $palabras->all()['palabras'];

        //reemplazamos las letras
        $cadena = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $cadena
        );

        $cadena = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $cadena );

        $cadena = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $cadena );

        $cadena = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $cadena );

        $cadena = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $cadena );

        $cadena = str_replace(
            array('ñ','Ñ','ç', 'Ç'),
            array('n','N','c', 'C'),
            $cadena
        );
        $cadena = str_replace(
            array('.', '-', ',', ';', '+', ':', '?', '¿', '¡', '!'),
            array(' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
            $cadena 
        );

        $cadena = trim($cadena);

        $aux=preg_split("/[\s,]+/", 
            str_replace(
            array('.', '-', ',', ';', '+', ':', '?', '¿', '¡', '!'),
            array(' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
            $palabras->all()['palabras'])
        );
        $palabrasArray=preg_split("/[\s,]+/", $cadena);
        $array=[];
        
        //no Borrar
        $text="";
        //no Borrar

     return view('extractors.sinonimosResult', ['text' => $text, 'palabras' => $aux, 'buscar' => $palabrasArray]);

    }

    public function sinonimosPalabra (Request $palabras){

        $client = new Goutte();
        $guzzleClient  = new Client(array(
            'timeout' => 1000,
        ));
        $client->setClient($guzzleClient );

            
        $aux = $palabras->all()['palabra'];
        $palabra = $palabras->all()['buscar'];

        $array=[];
        
       
       
            set_time_limit(300);
            $pos = strpos(strtolower($aux), 'n');
            $pos2 = strpos(strtolower($aux), 'ñ');
            //Se busca ti existen n y no ñ en la palabra 
            if ($pos !== false && $pos2 === false) {
                $crawler = $client->request('GET', 'https://www.sinonimosonline.com/'.$palabra.'-2/');
                $arrayAux = $crawler->filter('.synonim')->each(function ( $node, $i) {
                    $sentido = str_replace(':','',$node->filter('.sentido')->extract('_text'));
                    if(empty($sentido)) $sentido = array(' ');
                    return array_merge($sentido,$node->filter('.sinonimo')->extract('_text'));
                });
                if(empty($arrayAux)){
                    $crawler = $client->request('GET', 'https://www.sinonimosonline.com/'.$palabra.'/');
                    $arrayAux = $crawler->filter('.synonim')->each(function ( $node, $i) {
                    $sentido = str_replace(':','',$node->filter('.sentido')->extract('_text'));
                    if(empty($sentido)) $sentido = array(' ');
                    return array_merge($sentido,$node->filter('.sinonimo')->extract('_text'));
                });
                }

            } else {
                $crawler = $client->request('GET', 'https://www.sinonimosonline.com/'.$palabra.'/');
                $arrayAux = $crawler->filter('.synonim')->each(function ( $node, $i) {
                    $sentido = str_replace(':','',$node->filter('.sentido')->extract('_text'));
                    if(empty($sentido)) $sentido = array(' ');
                    return array_merge($sentido,$node->filter('.sinonimo')->extract('_text'));
                });
            }

            $array[strtolower($aux)]=$arrayAux;
            
        $text="";
        foreach ($array as $key => $sin) {
            foreach ($sin as $clave => $result) {
                $total="";
                foreach ($result as $index1 => $finalSinonimo) {
                    $total=$total.'; '.$finalSinonimo;
                }
               $text =$text.$key.$total."\n";
            }
        }

        return ['true' => 1, 'text' => $text];

    }
	
}