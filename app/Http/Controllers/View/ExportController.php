<?php
namespace App\Http\Controllers\View;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Goutte\Client as Goutte;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as RQ;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Maatwebsite\Excel\Facades\Excel;

class ExportController  extends Controller
{

	public function exportCSV(request $request)
	{
		
	    $title = "hola";
	    $title_japan["A"] = ["a","b","c","d","e"];
	    $title_japan["B"] = ["a","b","c","d"];
	    $title_japan["C"] = ["a","b","c","d"];
	    $title_japan["D"] = ["a","b","c","d"];
	    $title_japan["E"] = ["a","b","c","d"];


	    return Excel::create('Filename', function($excel) use ($title,$title_japan) {

	        $excel->setTitle('TitleBandai');
	        $excel->sheet("", function($sheet) use($title_japan) {
	        	//dd($title_japan);
	            //$sheet->fromArray($title_japan);

	            $sheet->row(1, array(
				     'test1', 'test2'
				));

				// Manipulate 2nd row
				$sheet->row(2, array(
				    'test3', 'test4'
				));
	        });

	    })->download('csv');
	}
}