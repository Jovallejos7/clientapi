<?php

namespace App\Http\Controllers\View;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Goutte\Client as Goutte;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as RQ;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

class ScheduleExtractionsViewController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    //Busca todas las programaciones personales de un usuario.
    public function AllSchedule(){
        $client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('GET',$this->api_url.'/api/schedule', $header);
            $response =  $client->send($rq);
            $extractors = json_decode($response->getBody(), true);
            $finalData=$extractors['data']['schedule'];

            return view('extractors.scheduleExtractions', ['schedule' => $finalData]);
        }catch (\Exception $ex){
            return "a ocurrido un error";
        }
    }

    //BUSCAR PROGAMACION POR ID
     public function findScheduleById($id){
        $client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('GET',$this->api_url.'/api/schedule/'.$id, $header);
            $response =  $client->send($rq);
            $extractors = json_decode($response->getBody(), true);
            $finalData['data']=$extractors['data']['schedule'];
            return $finalData;
        }catch (\Exception $ex){
            return "a ocurrido un error";
        }
    }

    //ELIMINAR PROGRAMACION
     public function deleteSchedule(request $request){
        $client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('POST',$this->api_url.'/api/schedule/delete', $header);
            $response =  $client->send($rq);
            $finalData = json_decode($response->getBody(), true);
            
            return $finalData;
        }catch (\Exception $ex){
            return "a ocurrido un error";
        }
    }


    //AÑADIR EXTRACTOR DIARIO
    public function addDailyExtractor(request $request){
        $dataError=array();
        $client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('POST',$this->api_url.'/api/schedule/daily/addExtractor', $header, json_encode($request->all()));
            $response =  $client->send($rq);
            $finalData = json_decode($response->getBody(), true);
        
            return ['true'=> 1, 'data' => $finalData];
        }catch (ClientException $ex){
            $dataError = json_decode((string)$ex->getResponse()->getBody()->getContents(),true);
            switch ($ex->getCode()) {
                case 404:
                    return view('simpleErrors.404');
                    break;
                case 500:
                    return view('simpleErrors.404');
                    break;
            }
            //Obtener json del error
            
        }

        return ['true' => 1, 'data' => $dataError];
    }

    //AÑADIR EXTRACTOR SEMANAL
    public function addWeeklyExtractor(request $request){
        $dataError=array();
        $client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('POST',$this->api_url.'/api/schedule/weekly/addExtractor', $header, json_encode($request->all()));
            $response =  $client->send($rq);
            $finalData = json_decode($response->getBody(), true);
        
            return ['true'=> 1, 'data' => $finalData];
        }catch (ClientException $ex){
            $dataError = json_decode((string)$ex->getResponse()->getBody()->getContents(),true);
            switch ($ex->getCode()) {
                case 404:
                    return view('simpleErrors.404');
                    break;
                case 500:
                    return view('simpleErrors.404');
                    break;
            }
            //Obtener json del error
            
        }

        return ['true' => 1, 'data' => $dataError];
    }

    //AÑADIR EXTRACTOR MENSUAL
    public function addMonthlyExtractor(request $request){
        $dataError=array();
        $client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('POST',$this->api_url.'/api/schedule/monthly/addExtractor', $header, json_encode($request->all()));
            $response =  $client->send($rq);
            $finalData = json_decode($response->getBody(), true);
        
            return ['true'=> 1, 'data' => $finalData];
        }catch (ClientException $ex){
            $dataError = json_decode((string)$ex->getResponse()->getBody()->getContents(),true);
            switch ($ex->getCode()) {
                case 404:
                    return view('simpleErrors.404');
                    break;
                case 500:
                    return view('simpleErrors.404');
                    break;
            }
            //Obtener json del error
            
        }

        return ['true' => 1, 'data' => $dataError];
    }


    //AÑADIR NODO DIARIO
    public function addDailyNode(request $request){
        $dataError=array();
        $client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('POST',$this->api_url.'/api/schedule/daily/addNode', $header, json_encode($request->all()));
            $response =  $client->send($rq);
            $finalData = json_decode($response->getBody(), true);
        
            return ['true'=> 1, 'data' => $finalData];
        }catch (ClientException $ex){
            $dataError = json_decode((string)$ex->getResponse()->getBody()->getContents(),true);
            switch ($ex->getCode()) {
                case 404:
                    return view('simpleErrors.404');
                    break;
                case 500:
                    return view('simpleErrors.404');
                    break;
            }
            //Obtener json del error
            
        }

        return ['true' => 1, 'data' => $dataError];
    }

    //AÑADIR NODO SEMANAL
    public function addWeeklyNode(request $request){
        $dataError=array();
        $client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('POST',$this->api_url.'/api/schedule/weekly/addNode', $header, json_encode($request->all()));
            $response =  $client->send($rq);
            $finalData = json_decode($response->getBody(), true);
        
            return ['true'=> 1, 'data' => $finalData];
        }catch (ClientException $ex){
            $dataError = json_decode((string)$ex->getResponse()->getBody()->getContents(),true);
            switch ($ex->getCode()) {
                case 404:
                    return view('simpleErrors.404');
                    break;
                case 500:
                    return view('simpleErrors.404');
                    break;
            }
            //Obtener json del error
            
        }

        return ['true' => 1, 'data' => $dataError];
    }

    //AÑADIR NODO MENSUAL
    public function addMonthlyNode(request $request){
        $dataError=array();
        $client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('POST',$this->api_url.'/api/schedule/monthly/addNode', $header, json_encode($request->all()));
            $response =  $client->send($rq);
            $finalData = json_decode($response->getBody(), true);
        
            return ['true'=> 1, 'data' => $finalData];
        }catch (ClientException $ex){
            $dataError = json_decode((string)$ex->getResponse()->getBody()->getContents(),true);
            switch ($ex->getCode()) {
                case 404:
                    return view('simpleErrors.404');
                    break;
                case 500:
                    return view('simpleErrors.404');
                    break;
            }
            //Obtener json del error
            
        }

        return ['true' => 1, 'data' => $dataError];
    }
	
}