<?php
namespace App\Http\Controllers\View;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Goutte\Client as Goutte;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as RQ;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Carbon\Carbon;


class UsersViewController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    //INFORMACION
    public function information ($login){
    	$client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('GET',$this->api_url.'/api/user/'.$login, $header);
            $response =  $client->send($rq);
            $info = json_decode($response->getBody(), true);
            $finalData=$info;
            return $finalData;

         }catch (\Exception $ex){
            return view('simpleErrors.error', ['error' => json_decode((string)$ex->getResponse()->getBody()->getContents(),true), 'code' => $ex->getCode()]);
        }
    }

    //INFORMACION
    public function MyInformation (){

        $client = new Client();
        
        try{
            $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
            $rq = new RQ('GET',$this->api_url.'/api/user/'.$_SESSION["SessionAPI"]['user']['login'], $header);
            $response =  $client->send($rq);
            $info = json_decode($response->getBody(), true);
            $finalData=$info['data']['user'];
            $finalData['registrationDate']= new Carbon($finalData['registrationDate']);
            $finalData['registrationDate'] =date("d-m-Y", strtotime($finalData['registrationDate']));
            
            return view('users.myInfo', ['info' => $finalData]);

        }catch (\Exception $ex){
            return view('accounts.activeAccount', ['user' => $_SESSION["SessionAPI"]['user']]);
        }
    }

    //EXTRACTORES DEL USUARIO
    public function extractorsByUser($login){
        $client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('GET',$this->api_url.'/api/user/'.$login.'/extractors', $header);
            $response =  $client->send($rq);
            $extractors = json_decode($response->getBody(), true);
            $finalData['data']=$extractors['data']['extractors'];
            return $finalData;
        }catch (\Exception $ex){
            return "a ocurrido un error";
        }
    }

     //EXTRACTORES DEL USUARIO Logeado
    public function MyExtractors(){
        $client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('GET',$this->api_url.'/api/user/'.$_SESSION["SessionAPI"]['user']['login'].'/extractors', $header);
            $response =  $client->send($rq);
            $extractors = json_decode($response->getBody(), true);
            $finalData['data']=$extractors['data']['extractors'];
            return $finalData;
        }catch (\Exception $ex){
            return "a ocurrido un error";
        }
    }
    //AGREGAR EXTRACTOR
    public function ViewAddExtractor(){
        return view('extractors.addExtractor');
    }

    public function ViewAddExtractorNew(){
        return view('extractors.addExtractorNew');
    }

    public function addExtractor(request $request){
        

        $data=$request->all();
        $extractor['extractorData']['name']= $data['name'];
        $extractor['extractorData']['url']= $data['url'];
        $extractor['extractorData']['description']= $data['description'];
        $extractor['extractorData']['nodes'] = [];

        if(isset($data['shared'])){
            if($request->all()['shared']=="on"){
                $extractor['extractorData']['shared']=true;
            }
            else{
                $extractor['extractorData']['shared']=false;
            }
        }
        else{
                $extractor['extractorData']['shared']=false;
            }
        


        for ($i=1; $i < 11 ; $i++) { 
            if(isset($data['name-'.$i])){

                $node['name']=$data['name-'.$i];
                $node['description']=$data['description-'.$i];
                $node['container']=$data['container-'.$i];
                $node['elements'] = [];

                for ($j=1; $j < 11 ; $j++) { 
                    if(isset($data['tag-'.$j.'-'.$i])){
                        $element['tag'] = $data['tag-'.$j.'-'.$i];
                        $element['selector'] = $data['selector-'.$j.'-'.$i];
                        $node['elements'][] = $element;
                    }
                    
                }
                $extractor['extractorData']['nodes'][] = $node;
            }
            
        }

        $client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('POST',$this->api_url.'/api/user/extractors/add', $header, json_encode($extractor));
            $response =  $client->send($rq);
            $result = json_decode($response->getBody(), true);
            

            return redirect()->route('extractors.myextractors');
        }catch (\Exception $ex){
            return $ex;
        }
    }

    //MODIFICAR EXTRACTOR
    public function updateExtractor(request $request){
        $client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('POST',$this->api_url.'/api/user/extractors/update', $header, json_encode($request->all()));
            $response =  $client->send($rq);
            $result = json_decode($response->getBody(), true);
            

            return $result;
        }catch (\Exception $ex){
            return $ex;
        }
    }

    //ELIMINAR EXTRACTOR
     public function deleteExtractor(request $request){
     	$client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('POST',$this->api_url.'/api/user/extractors/delete', $header, json_encode($request->all()));
            $response =  $client->send($rq);
            $result = json_decode($response->getBody(), true);
            return ['true' => 1, 'data' => $result];

            return $result;
        }catch (\Exception $ex){
            return $ex;
        }
    }
	
}