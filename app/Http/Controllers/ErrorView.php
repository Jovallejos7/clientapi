<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as RQ;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use App\Http\Controllers\Helpers\Helpers;


class ErrorView extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | ErrorView Controller
    |--------------------------------------------------------------------------

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function error404 (){

        return view('simpleErrors.404', ['error' => 'none']);
    }

    public function error500 (){

        return view('simpleErrors.500', ['error' => 'none']);
    }

    public function noPage (){

        return view('simpleErrors.noPage', ['error' => 'none']);
    }

    

 	
	
}