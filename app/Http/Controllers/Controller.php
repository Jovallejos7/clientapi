<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $api_url = null;
    protected $header_api = null;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->api_url = config('constants.api_url');
            $this->header_api = ['Content-Type' => 'application/json'];
            date_default_timezone_set('America/Santiago');
            session_start();
            
            //URLS QUE NO NECESITAN AUTENTICACIÓN de lo contrario se envia al login
            if($request->path()!="login" && $request->path()!="/" && $request->path()!="api/loginAuth" && $request->path()!="extractors/extractorsPublic" && $request->path()!="extractors/executeExtractorPublic" && $request->path()!="extractors/loadAllExtractorsPublic" && $request->path()!="api/forgot" && $request->path()!="api/register" && $request->path()!="api/activation" && $request->path()!="api/reset" ){
                if (empty($_SESSION["SessionAPI"])) {
                    return redirect('/login');
                }
            }
            if($request->path()=="login"){
                if (!empty($_SESSION["SessionAPI"])) {
                    return redirect('/start/home');
                }
            }
            return $next($request);
        });
    }

}
