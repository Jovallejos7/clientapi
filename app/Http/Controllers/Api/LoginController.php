<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as RQ;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use App\Http\Controllers\Helpers\Helpers;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index (){

        return view('accounts.newLogin', ['error' => '']);
    }

 	public function login(Request $request)
	{       //dd($request->all());
    	$client = new Client();
        $header = ['x-access-token'=>"", 'Content-Type'=> 'application/json'];
        $rq = new RQ('POST',$this->api_url.'/api/login', $header, json_encode($request->all()));
        try{
            $response =  $client->send($rq);
            $user = json_decode($response->getBody(), true);
            
            $_SESSION["SessionAPI"]=$user['data'];
           
             
            return redirect()->route('start.home');
        }catch (\Exception $ex){
            return view('accounts.newLogin', ['error' => json_encode(json_decode($ex->getResponse()->getBody(), true)['errorsContent'])]);
        }
	}



    public function register(Request $request)
    {       //dd($request->all());
        $client = new Client();
        $header = ['x-access-token'=>"", 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('POST',$this->api_url.'/api/signup', $header, json_encode($request->all()));
            $response =  $client->send($rq);
            $data = json_decode($response->getBody(), true);
            return ['true' => 1, 'data' => $data];
        }catch (\Exception $ex){
            return ['true' => 1, 'data' => json_decode($ex->getResponse()->getBody(), true)];
        }
        
    }

    public function activateView(Request $request)
    {       //dd($request->all());
            return view('accounts.activeAccount');
    }

    public function activation(Request $request)
    {       
        $client = new Client();
        $header = ['x-access-token'=>"", 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('POST',$this->api_url.'/api/activation', $header, json_encode($request->all()));
            $response =  $client->send($rq);
            $user = json_decode($response->getBody(), true);
            return ['true' => 1, 'data' => $user];
        }catch (\Exception $ex){
            return ['true' => 1, 'data' => json_decode($ex->getResponse()->getBody(), true)];
        }
    
    }

    //REINICIO DE CONTRASEÑA
    public function forgot(Request $request)
    {   
        $client = new Client();
        $header = ['x-access-token'=>"", 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('POST',$this->api_url.'/api/forgot', $header, json_encode($request->all()));
            $response =  $client->send($rq);
            $resp = json_decode($response->getBody(), true);
            return ['true' => 1, 'data'=>$resp];
        }catch (\Exception $ex){
            return ['true' => 1, 'data' => json_decode($ex->getResponse()->getBody(), true)];
        }
        
    }

    //REINICIO DE CONTRASEÑA
    public function reset(Request $request)
    {       //dd($request->all());
        $client = new Client();
        $header = ['x-access-token'=>"", 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('POST',$this->api_url.'/api/reset', $header, json_encode($request->all()));
            $response =  $client->send($rq);
            $data = json_decode($response->getBody(), true);
            return ['true' => 1, 'data'=>$data];
        }catch (\Exception $ex){
            return ['true' => 1, 'data' => json_decode($ex->getResponse()->getBody(), true)];
        }
    }

    

    //REINICIO MAIL NO VALIDO
    public function invalid(Request $request)
    {      
        $datos=$request->all();
        $datos['login']=$_SESSION["SessionAPI"]['user']['login'];
        $datos['email']=$_SESSION["SessionAPI"]['user']['email']; 
        $client = new Client();
        $header = ['x-access-token'=>$_SESSION["SessionAPI"]['token'], 'Content-Type'=> 'application/json'];
        try{
            $rq = new RQ('POST',$this->api_url.'/api/invalid', $header, json_encode($datos));
            $response =  $client->send($rq);
            $user = json_decode($response->getBody(), true);
            return ['true' => 1, 'data'=>$data];
        }catch (\Exception $ex){
            return ['true' => 1, 'data' => json_decode($ex->getResponse()->getBody(), true)];
        }
        
    }

    public function logout()
    {       //dd($request->all());
        session_unset();
        return redirect()->route('login');
    }
	
}