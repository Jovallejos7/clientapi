var search_by_tag = search_by_tag || function () {
    var _bar = $('div.autocomplete-popover');
    var autocomplete_id = _bar.attr('id');
    return {
        bind: function(){ 
            _bar.autocomplete({
                minLength: 1,
                autofocus: true,
                source: function( request, response ) {
                    var lastword = request.term.split( / \s*/ ).pop();
                    var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( lastword ), "i" );
                    var labels = db_users.get_users_data().source.map(function(item){return item.label;});
                    var results = $.grep(labels ,function( item ){ return matcher.test( item ); });
                    response( $.ui.autocomplete.filter( results, lastword.replace('@','')));
                },
                messages: {
                    noResults: 'No se han encontrado Usuarios',
                    results: function() {
                        $("ul.ui-autocomplete").css({"max-height":"150px" , "overflow-y":"auto"});
                    }
                },
                search: function(event, ui){
                    console.log(ui);
                },
                select: function(evento, ui){
                    var span_id = Math.random();
                    var username = ui.item.label.replace('@','');
                    _bar.html(_bar.html()+' <b class="b-tag-user" username="'+username+'">'+username+'</b>');
                }
            }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                var inner_html = 
                '   <div class="">'+
                '       <div>'+
                // '           <img src="' + item.imgsrc + '" onerror="this.onerror=null;this.src=\''+item.imgerror+'\'" class="img-circle img-sm" style="margin-top: 15px">'+
                '       </div>'+
                '       <div>'+
                '           <h5>' + item.label  + '</h5>'+
                '       </div>'+
                '   </div>';
                return $( "<li></li>" )
                    .data( "item.autocomplete", item )
                    .append(inner_html)
                    .appendTo( ul );
            };
        }
    };
}();