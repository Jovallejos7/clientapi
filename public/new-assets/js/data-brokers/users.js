var db_users = db_users || (function(){
    var _args = {}; // private
    var _users_data = [];

    return {
        init : function(Args) {
            _args = Args;
            $.ajax({
                async: true,
                type: 'get',
                url: _args[0],
                success: function (data) {
                    _users_data['source'] = data;
                    search_bar.bind();
                    // search_by_tag.bind();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        },
        get_users_data : function () {
            return _users_data;
        }
    };
}());