$(function () {
	$('#calendar').fullCalendar({
  		locale: 'es',
      	header: {
        	left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
      	},
      	buttonText: {
            today: 'Hoy',
            month: 'Mes',
            week: 'Semana',
            day: 'Día'
      	},
      	//Random default events
      	// events: {!!json_encode($events)!!},
      	editable: false,
      	eventClick: function(event, jsEvent, view) {
            $body = $("body");
            $body.addClass("loading");
            var url = '';
            url = url.replace(':id:', event.id);
            $.ajax({
          		data: {},
              	type: "GET",
              	dataType: "json",
              	url: url
            }).done(function( data) {
              	$start = moment(data.start);
              	$end = moment(data.end);
              	$body.removeClass("loading");
              	swal({
                	title: data.title,
                	type: null,
                	width: 800,
                	imageUrl: data.image,
                	animation: false,
                	html:
                		(data.description!=null ? '<div class="row"><div class="col-sm-3 col-xs-5 text-right no-padding"><b>Descripción: </b></div><div class="col-sm-9 col-xs-7 text-left">'+data.description + '</div></div>' : '') +
                		(data.location!=null ? '<div class="row"><div class="col-sm-3 col-xs-5 text-right no-padding"><b>Ubicación: </b></div><div class="col-sm-9 col-xs-7 text-left"> '+data.location + '</div></div>' : '') +
                		'<div class="row"><div class="col-sm-3 col-xs-5 text-right no-padding"><b>Fecha Inicio: </b></div><div class="col-sm-9 col-xs-7 text-left"> ' + $start.format('DD/MM/YYYY hh:mm a') + '</div></div>' +
                		'<div class="row"><div class="col-sm-3 col-xs-5 text-right no-padding"><b>Fecha Fin: </b></div><div class="col-sm-9 col-xs-7 text-left"> ' + $end.format('DD/MM/YYYY hh:mm a') + '</div></div>' +
                		'<div class="row"><div class="col-sm-3 col-xs-5 text-right no-padding"><b>Ir a: </b></div><div class="col-sm-9 col-xs-7 text-left"><a href="'+data.link+'" target="_blank"> Google Calendar </a></div></div>' ,
                	showCloseButton: true,
                	// showCancelButton: @can('create.event') true @else false  @endcan,
                	showLoaderOnConfirm: true,
                	confirmButtonText: '<i class="fa fa-thumbs-up"></i> OK!',
                	cancelButtonText: '<i class="fa fa-trash"></i> Eliminar!'
              	}).then(function () {}, 
              		function (dismiss) {
                		if (dismiss === 'cancel') {
                  			var url = $url;
                  			url = url.replace(':id:', event.id);
                  			location.href=url;
                		}
              		});
            	});
          	}
        });
	});
	moment.locale('es');
    $('#datesEvents').daterangepicker({linkedCalendars: false, locale: {
    	format: 'DD/MM/YYYY', applyLabel: 'Aplicar', cancelLabel: 'Cancelar'
  	}});

  	$('#datetimesEvents').daterangepicker({linkedCalendars: false, timePicker: true, timePickerIncrement: 15,locale: {
    	format: 'DD/MM/YYYY h:mm A', applyLabel: 'Aplicar', cancelLabel: 'Cancelar'
  	}});

    $('#eventColor').simplecolorpicker();

    $("#all_day").change(function() {
        if(this.checked) {
          	$("#only_date").removeClass("hidden");
          	$("#date_time").addClass("hidden");
          	$("#only_date").attr('required', '');
          	$("#date_time").removeAttr('required');

          	$("#datesEvents").data('daterangepicker').setStartDate($("#datetimesEvents").data('daterangepicker').startDate);
          	$("#datesEvents").data('daterangepicker').setEndDate($("#datetimesEvents").data('daterangepicker').endDate);
          	$('#datesEvents').datepicker('updateDates');
        }
        else{
          	$("#only_date").addClass("hidden");
          	$("#date_time").removeClass("hidden");
          	$("#only_date").removeAttr('required');
          	$("#date_time").attr('required', '');

          	$("#datetimesEvents").data('daterangepicker').setStartDate($("#datesEvents").data('daterangepicker').startDate);
          	$("#datetimesEvents").data('daterangepicker').setEndDate($("#datesEvents").data('daterangepicker').endDate-1);
          	$('#datetimesEvents').datepicker('updateDates');
        }
      });
});