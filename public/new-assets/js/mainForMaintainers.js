var search_bar = search_bar || function () {
    var _args = {}; // private
    var _modal_url = "";
    var _bar = $('input.autocomplete');

    return {
        init: function (Args) {
            _args = Args;
            _modal_url = _args[0];
        },
        bind: function(){
            _bar.autocomplete(db_users.get_users_data())
                .data('ui-autocomplete')._renderItem = function( ul, item ) {
                var inner_html = '<div class="media box-comment box-tag" style="margin-top: 0;">' +
                    '        <div class="user-block m-t--20" style="padding: 5px;">' +
                    '            <img src="' + item.imgsrc + '" onerror="this.onerror=null;this.src=\''+item.imgerror+'\'" class="img-circle img-sm" style="margin-top: 15px">' +
                    '        </div>&nbsp;&nbsp;' +
                    '        <div class="media-body p-l-10">' +
                    '            <h5 class="media-heading col-blue-grey">' + item.label.replace('@','') + '</h5>' +
                    '           </div>' +
                    '        </div>';

                return $( "<li></li>" )
                    .data( "item.autocomplete", item )
                    .append(inner_html)
                    .appendTo( ul );
            };

            _bar.keyup(function (e) {
                e.preventDefault();
                if (e.keyCode === 13) {
                    search_bar.modal_users();
                }
            });
        },
        modal_users: function () {
            var username = "@"+_bar.val();
            var url = _modal_url.replace(':username:', username);
            $("#modal-body-user").html("<div style='height: 100px;'></div>");
            var $loading = $("#modal-body-user").waitMe({
                effect: 'win8_linear',
                text: 'Cargando...',
                bg: 'rgba(255,255,255,1)',
                color: '#555'
            });
            $.ajax({
                type: 'GET',
                async: true,
                url: url,
                dataType: 'json',
                beforeSend: function () {
                    // CAMBIO DE COLOR EN BASE A COLOR GUARDADO EN BD
                    var color_rgba = $('#content-modal-user').css('background-color');
                    var class_color = $('#content-modal-user').attr('class');
                    var rgb = color_rgba.match(/\d+/g);
                    var array = class_color.split(' ');
                    for (var i = array.length - 1; i >= 0; i--) {
                        if (array[i].includes("bg-")) {
                            $('#content-modal-user').removeClass(array[i]).css('background-color', 'rgba(' + (parseInt(rgb[0]) + 10) + ',' + (parseInt(rgb[1]) - 15) + ',' + (parseInt(rgb[2]) + 10) + ', 0.8)');
                        }
                    }
                    $(".close-search").click();
                    $('#modal-user').modal({show: true, keyboard: true});
                },
                success: function (data) {
                    $("#modal-body-user").html(data.html);
                    $loading.waitMe('hide');
                },
                error: function (e) {
                    $("#modal-body-user").html("<h4 align='center' style='color: white;'>Ha ocurrido un error al mostrar la información, por favor intente más tarde.</h4>");
                    $loading.waitMe('hide');
                }
            });
        }
    };
}();