function Valida_Rut( Objeto )
{
    var tmpstr = "";
    var intlargo = Objeto.value;
    var dv;
    var dvCal;
    intlargo = intlargo.replace('.','');
    if(intlargo.indexOf("-")>=0)
    {
        var array = intlargo.split("-");
        intlargo = array[0];
        dv = array[1];
        var sum = 0;
        var d = 2;
        intlargo = intlargo.split("").reverse();
        for(var i = 0; i<intlargo.length; i++)
        {
            if(d>7)
            {
                d = 2;
            }

            var a = intlargo[i]*d
            sum+=a;
            d++;
        }
        sum = 11-(sum%11);

        if(sum === 10)
        {
            dvCal = 'K';
        }
        else
        {
            if(sum == 11)
            {
                dvCal = '0';
            }
            else
            {
                dvCal = sum;
            }
        }
        if(dv != dvCal)
        {
            swal('Rut NO Valido');
        }
    }
}
function checkRut(Objeto)
{
    var tmpstr = "";
    var intlargo = Objeto.value;
    if (intlargo.length > 0 && intlargo.charAt(0) != 'P' && intlargo.charAt(0) != 'p')
    {
        crut = Objeto.value
        largo = crut.length;
        if ( largo < 2 )
        {
            //Objeto.focus() //para obligar al usuario a ingresar un rut correcto
            //TODO class "has-error"
            document.getElementById("rutdiv").classList.add('has-error');
            document.getElementById('rutlbl').innerHTML = 'Rut Invalido';
            return 0;
        }
        for ( i=0; i <crut.length ; i++ )
            if ( crut.charAt(i) != ' ' && crut.charAt(i) != '.' && crut.charAt(i) != '-' )
            {
                tmpstr = tmpstr + crut.charAt(i);
            }
        crut = tmpstr;
        largo = crut.length;

        if ( largo > 2 )
            rut = crut.substring(0, largo - 1);
        else
            rut = crut.charAt(0);

        dv = crut.charAt(largo-1);

        if ( rut == null || dv == null ){
            document.getElementById("rutdiv").classList.add('has-error');
            document.getElementById('rutlbl').innerHTML = 'Rut Invalido';
            return 0;
        }
        var dvr = '0';
        suma = 0;
        mul  = 2;

        for (i = rut.length-1 ; i>= 0; i--)
        {
            suma = suma + rut.charAt(i) * mul;
            if (mul == 7)
                mul = 2;
            else
                mul++;
        }

        res = suma % 11;
        if (res==1)
            dvr = 'k';
        else if (res==0)
            dvr = '0';
        else
        {
            dvi = 11-res;
            dvr = dvi;
        }

        if ( dvr != dv.toLowerCase() )
        {
            //Objeto.focus()
            document.getElementById("rutdiv").classList.add('has-error');
            document.getElementById('rutlbl').innerHTML = 'Rut Invalido';
            return 0;
        }
        //Objeto.focus()
        var dcformat = new DecimalFormat("#,##0");
        lrut = dcformat.format(rut)+'-'+dv;
        lrut = lrut.replace(/,/g , ".");
        Objeto.value = lrut;
    }
    document.getElementById("rutdiv").classList.remove('has-error');
    document.getElementById('rutlbl').innerHTML = '';
    return 1;
}
