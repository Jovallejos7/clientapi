function generate_charts( url ) {
    $('.sparkpie').sparkline('html', {
        type: 'pie',
        height: '20px',
        sliceColors: [
            '#0576ba',
            '#ff1981',
            '#fc8f55',
            '#ffee0c',
            '#777',
            '#db242c'
        ],
        tooltipFormat: '<img src="{{offset:icons}}" class="img-zentin"> {{offset:labels}}: {{value}}',
        tooltipValueLookups: {
            labels: [
                'Me gusta',
                'Me encanta',
                'Me divierte',
                'Me asombra',
                'Me entristece',
                'Me enoja'
            ],
            icons: [
                url+'/guino.png',
                url+'/me_encanta.png',
                url+'/alegre.png',
                url+'/sorprendido.png',
                url+'/triste.png',
                url+'/enojado.png'
            ]
        }
    });
}

function generate_chartByIdInModal( className,  url ) {
    $('.'+className).sparkline('html', {
        type: 'pie',
        height: '2em',
        sliceColors: [
            '#0576ba',
            '#ff1981',
            '#fc8f55',
            '#ffee0c',
            '#777',
            '#db242c'
        ],
        tooltipFormat: '<img src="{{offset:icons}}" class="img-zentin"> {{offset:labels}}: {{value}}',
        tooltipValueLookups: {
            labels: [
                'Me gusta',
                'Me encanta',
                'Me divierte',
                'Me asombra',
                'Me entristece',
                'Me enoja'
            ],
            icons: [
                url+'/guino.png',
                url+'/me_encanta.png',
                url+'/alegre.png',
                url+'/sorprendido.png',
                url+'/triste.png',
                url+'/enojado.png'
            ]
        }
    });
}

function generate_chartByIdInInfiniteScroll( className,  url ) {
    $('.'+className).sparkline('html', {
        type: 'pie',
        height: '20px',
        sliceColors: [
            '#0576ba',
            '#ff1981',
            '#fc8f55',
            '#ffee0c',
            '#777',
            '#db242c'
        ],
        tooltipFormat: '<img src="{{offset:icons}}" class="img-zentin"> {{offset:labels}}: {{value}}',
        tooltipValueLookups: {
            labels: [
                'Me gusta',
                'Me encanta',
                'Me divierte',
                'Me asombra',
                'Me entristece',
                'Me enoja'
            ],
            icons: [
                url+'/guino.png',
                url+'/me_encanta.png',
                url+'/alegre.png',
                url+'/sorprendido.png',
                url+'/triste.png',
                url+'/enojado.png'
            ]
        }
    });
    $('.'+className).removeClass(className);
}

function generate_chartById( className, url,array ) {
    $('.'+className).sparkline(array, {
        type: 'pie',
        height: '20px',
        sliceColors: [
            '#0576ba',
            '#ff1981',
            '#fc8f55',
            '#ffee0c',
            '#777',
            '#db242c'
        ],
        tooltipFormat: '<img src="{{offset:icons}}" class="img-zentin"> {{offset:labels}}: {{value}}',
        tooltipValueLookups: {
            labels: [
                'Me gusta',
                'Me encanta',
                'Me divierte',
                'Me asombra',
                'Me entristece',
                'Me enoja'
            ],
            icons: [
                url+'/guino.png',
                url+'/me_encanta.png',
                url+'/alegre.png',
                url+'/sorprendido.png',
                url+'/triste.png',
                url+'/enojado.png'
            ]
        }
    });
}
function generate_byLoading( url ) {
    var sparks = $('.sparkpie_loaded');
    sparks.sparkline('html', {
        type: 'pie',
        height: '2em',
        sliceColors: [
            '#0576ba',
            '#ff1981',
            '#fc8f55',
            '#ffee0c',
            '#777',
            '#db242c'
        ],
        tooltipFormat: '<img src="{{offset:icons}}" class="img-zentin"> {{offset:labels}}: {{value}}',
        tooltipValueLookups: {
            labels: [
                'Me gusta',
                'Me encanta',
                'Me divierte',
                'Me asombra',
                'Me entristece',
                'Me enoja'
            ],
            icons: [
                url+'/guino.png',
                url+'/me_encanta.png',
                url+'/alegre.png',
                url+'/sorprendido.png',
                url+'/triste.png',
                url+'/enojado.png'
            ]
        }
    });
    sparks.addClass('sparkpie');
    sparks.removeClass('sparkpie_loaded');
}