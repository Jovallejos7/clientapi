<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('loginAuth','Api\LoginController@login')->name('loginAuth');
Route::post('register','Api\LoginController@register')->name('registerUser');
Route::post('activation','Api\LoginController@activation')->name('activation');
Route::post('forgot', 'Api\LoginController@forgot')->name('forgot.password');
Route::get('logout','Api\LoginController@logout')->name('logout');
Route::post('reset', 'Api\LoginController@reset')->name('reset.password');
