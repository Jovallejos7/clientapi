<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', 'Api\LoginController@index')->name('login');


Route::prefix('start')->group(function () {
        Route::get('home', 'View\StartViewController@home')->name('start.home');
    });

Route::prefix('extractors')->group(function () {

    Route::get('extractors', 'View\ExtractorsViewController@extractors')->name('extractors.extractors');
    Route::get('loadAllExtractors', 'View\ExtractorsViewController@loadAllExtractors')->name('extractors.loadAllExtractors');
    Route::post('executeExtractor', 'View\ExtractorsViewController@executeExtractor')->name('extractors.executeExtractor');
    Route::get('optenerdatos', 'View\ExtractorsViewController@optenerdatos')->name('optenerdatos');
    Route::get('findExtractorById/{id}', 'View\ExtractorsViewController@findExtractorById')->name('extractors.findExtractorById');
    Route::get('findExtractorByName/{name}', 'View\ExtractorsViewController@findExtractorByUrl')->name('extractors.findExtractorByUrl');
    Route::get('findExtractorByUrl/{url}', 'View\ExtractorsViewController@extractors')->name('extractors.findExtractorByUrl');
    Route::post('TestExtractor', 'View\ExtractorsViewController@TestExtractor')->name('extractors.TestExtractor');
    Route::post('executeNode', 'View\ExtractorsViewController@executeNode')->name('extractors.executeNode');
    Route::post('testNode', 'View\ExtractorsViewController@testNode')->name('extractors.testNode');

    //todo Publico
    Route::get('extractorsPublic', 'View\ExtractorsViewController@extractorsPublic')->name('extractors.extractorsPublic');
    Route::get('loadAllExtractorsPublic', 'View\ExtractorsViewController@loadAllExtractorsPublic')->name('extractors.loadAllExtractorsPublic');
    Route::post('executeExtractorPublic', 'View\ExtractorsViewController@executeExtractorPublic')->name('extractors.executeExtractorPublic');
});


     Route::get('chancheEmail', 'Api\LoginController@invalid')->name('chancheEmail');


Route::prefix('twitter')->group(function(){
     Route::get('twitter', 'View\ExtractorsViewController@twitter')->name('extractors.twitter');
    Route::get('executeTwitterExtractor/{query}', 'View\ExtractorsViewController@executeTwitterExtractor')->name('extractors.executeTwitterExtractor');
    Route::get('executeTwitterExtractorNull', 'View\ExtractorsViewController@executeTwitterExtractorNull')->name('extractors.executeTwitterExtractorNull');

    Route::get('executeTwitterSearch', 'View\ExtractorsViewController@executeTwitterExtractorSearchMain')->name('extractors.twitterSearchMain');

});

Route::prefix('users')->group(function () {

    Route::get('myExtractors', 'View\ExtractorsViewController@myExtractors')->name('extractors.myextractors');
    Route::get('myExtractors/data', 'View\UsersViewController@MyExtractors')->name('extractors.myextractorsdata');  

    Route::get('information/{login}', 'View\UsersViewController@information')->name('users.informacion');

    Route::get('MyInformation', 'View\UsersViewController@MyInformation')->name('users.MiInformacion');
    Route::get('ViewAddExtractor', 'View\UsersViewController@ViewAddExtractor')->name('users.ViewAddExtractor');
    Route::post('addExtractor', 'View\UsersViewController@addExtractor')->name('users.addExtractor');


    
    Route::post('updateExtractor', 'View\UsersViewController@updateExtractor')->name('users.updateExtractor');
    Route::get('deleteExtractor', 'View\UsersViewController@deleteExtractor')->name('users.deleteExtractor');

    //obtener nodos
    Route::get('ViewAddExtractorNew', 'View\UsersViewController@ViewAddExtractorNew')->name('users.ViewAddExtractorNew');
    Route::get('getNodes', 'View\ExtractorsViewController@getNodes')->name('users.getNodes');
    //obtener elemntos
    Route::get('getElements', 'View\ExtractorsViewController@getElements')->name('users.getElements');



});

Route::prefix('schedule')->group(function () {

    Route::get('AllSchedule', 'View\ScheduleExtractionsViewController@AllSchedule')->name('schedule.AllSchedule');
    Route::get('findScheduleById/{id}', 'View\ScheduleExtractionsViewController@findScheduleById')->name('schedule.findScheduleById');  
    Route::post('deleteSchedule', 'View\ScheduleExtractionsViewController@deleteSchedule')->name('schedule.deleteSchedule');
    
    Route::get('addDailyExtractor', 'View\ScheduleExtractionsViewController@addDailyExtractor')->name('schedule.addDailyExtractor');
    
    Route::get('addWeeklyExtractor', 'View\ScheduleExtractionsViewController@addWeeklyExtractor')->name('schedule.addWeeklyExtractor');
    
    Route::get('addMonthlyExtractor', 'View\ScheduleExtractionsViewController@addMonthlyExtractor')->name('schedule.addMonthlyExtractor');
    Route::post('addDailyNode', 'View\ScheduleExtractionsViewController@addDailyNode')->name('schedule.addDailyNode');
    Route::post('addWeeklyNode', 'View\ScheduleExtractionsViewController@addWeeklyNode')->name('schedule.addWeeklyNode');
    Route::post('addMonthlyNode', 'View\ScheduleExtractionsViewController@addMonthlyNode')->name('schedule.addMonthlyNode');

});

Route::prefix('error')->group(function () {

    Route::get('404', 'ErrorView@error404')->name('error.404');
    Route::get('500', 'ErrorView@error500')->name('error.500');
    Route::get('noPage', 'ErrorView@noPage')->name('error.noPage');

});

Route::prefix('export')->group(function () {

    Route::post('csv', 'View\ExportController@exportCSV')->name('exportCSV');

});

Route::post('sinonimosExtractor', 'View\ExtractorsViewController@sinonimosExtractor')->name('sinonimosExtractor');
Route::get('sinonimosExtractorView', 'View\ExtractorsViewController@sinonimosExtractorView')->name('sinonimosExtractorView');
Route::post('sinonimosPalabra', 'View\ExtractorsViewController@sinonimosPalabra')->name('sinonimosPalabra');
