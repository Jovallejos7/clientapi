<!DOCTYPE html>
<html lang="es">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <title>Text Extraction Community</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{ asset('new-style/vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('new-style/vendors/css/vendor.bundle.base.css') }}">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{ asset('new-style/css/style.css') }}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{ asset('new-style/images/somosmini.png') }}">
  <!-- Personal -->
  <link rel="stylesheet" type="text/css" href="{{ asset('dist/css/sweetalert2.min.css') }}">
  
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
  

  <style type="text/css">
    .hidden {
      display: none !important;
    }
  </style>
</head>

<body>
  <div class="container-scroller">
    <!-- partial:../../partials/_navbar.html -->
    <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo" href="{{route('start.home')}}"><img src="{{ asset('new-style/images/somos.png') }}" alt="logo"/></a>
        <a class="navbar-brand brand-logo-mini"><img src="{{ asset('new-style/images/somosmini.png') }}" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-stretch">
        <ul class="navbar-nav navbar-nav-right">
          <button type="button" onclick="location.href='{{route('login')}}'" class="btn btn-outline-info btn-fw">Ingresar</button>
        </ul>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      
      <!-- partial -->
      <div style="width: 100%" class="main-panel">
        <div class="content-wrapper">
        	<div class="page-header">
		  <h3 class="page-title">
		    <span class="page-title-icon bg-gradient-info text-white mr-2">
		      <i class="mdi mdi-clipboard-text"></i>                 
		    </span>
		    Extractores
		  </h3>
		</div>

   <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <div class="table-responsive">
                  <table id="tableId" class="table table-striped">
                    <thead>
                      <tr>
                        <tr>
                            <th class="hidden">extractorId</th>
                            <th>Extractor</th>
                            <th>Descripción</th>
                            <th>URL</th>
                            <th>Ejecutar</th>
                        </tr>
                      </tr>
                   
                    </thead>
                  </table>
                  </div>
                </div>
              </div>
            </div>

        </div>
        
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="https://dsi.face.ubiobio.cl/somos/" target="_blank">SOMOS U.B.B</a>. Todos los derechos reservados.</span>
            <!--span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hecho Con <i class="mdi mdi-heart text-danger"></i></span-->
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="{{ asset('new-style/vendors/js/vendor.bundle.base.js') }}"></script>
  <script src="{{ asset('new-style/vendors/js/vendor.bundle.addons.js') }}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="{{ asset('new-style/js/off-canvas.js') }}"></script>
  <script src="{{ asset('new-style/js/misc.js') }}"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <!-- End custom js for this page-->
  <script type="text/javascript" src="{{ asset('dist/js/sweetalert2.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jQuery/jquery-3.1.1.min.js') }}"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
  <script>
        $(document).ready(function() {


           
           

           console.log("hola");
            //console.log(JSON.stringify(collaborators));
            $('#tableId').DataTable( {
                language: {
                    sProcessing:     "Cargando datos...&nbsp;&nbsp;<img height='32' width='32' src='{{url('dist/img/loader.gif')}}'>",
                    sLengthMenu:     "Mostrar _MENU_ registros",
                    sZeroRecords:    "No se encontraron resultados",
                    sEmptyTable:     "No existe ningún registro en este momento",
                    sInfo:           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    sInfoEmpty:      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    sInfoFiltered:   "(filtrado de un total de _MAX_ registros)",
                    sInfoPostFix:    "",
                    sSearch:         "Buscar:",
                    sUrl:            "",
                    sInfoThousands:  ",",
                    sLoadingRecords: "&nbsp;",
                    oPaginate: {
                        sFirst:    "Primero",
                        sLast:     "Último",
                        sNext:     "Siguiente",
                        sPrevious: "Anterior"
                    },
                    oAria: {
                        sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
                        sSortDescending: ": Activar para ordenar la columna de manera descendente"
                    }
                },
                ajax: {
                    url: "{{route('extractors.loadAllExtractorsPublic')}}",
                    type: "GET",
                    beforeSend: function (request) {
                        request.setRequestHeader("X-CSRF-TOKEN", $('input[name="_token"]').val());
                    },
                },
                    //Employee dates: picture, name and postulating button//
                columns: [
                    { data: "id", class: "hidden", defaultContent: "default" },
                    { data: "name", defaultContent: "default" },
                    { data: "description", defaultContent: "default"  },
                    { data: "url",className: "center",
                      mRender: function (data, type, full) {
                        console.log(data);
                        return '<a href="'+data+'" target="_blank">'+data+'</a>'
                      }
                    },
                    { data: "id", className: "center",
                      mRender: function (data, type, full) {
                        console.log(data);
                        var myUrl = '{{route('extractors.executeExtractorPublic')}}';
                        // myUrl = myUrl.replace(':id:',data);
                        return '<form method="post" action='+`"`+myUrl+`"`+'> {{csrf_field()}} <input class="hidden" type="text" name="id" value='+`"`+data+`"`+'> <input class="hidden" type="text" name="url" value='+`'`+full.url+`'`+'> <button type="submit" class="btn btn-gradient-success btn-rounded btn-icon"><i class="mdi mdi-play-circle btn-icon-prepend"></i></button> </form> '
                      }
                    },
                ],
                paging: true,
                lengthChange: true,
                searching: true,
                ordering: true,
                info: true,
                autoWidth: true,
                processing: true,
                //order: [[ 12, "desc" ]],
               
            });

            
        });
</script>
</body>

</html>
