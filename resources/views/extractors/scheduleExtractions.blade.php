@extends('layout.newMain')
@section('content')
<style>


td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
<div class="page-header">
  <h3 class="page-title">
    <span class="page-title-icon bg-gradient-info text-white mr-2">
      <i class="mdi mdi-meteor"></i>                 
    </span>
   Extracciones Programadas
  </h3>
  
</div>

<div style="overflow: auto;">
@foreach($schedule as $key => $extract)
 <div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
              <table class="table table-striped table-sm">
                <tr>
                  	<th><strong>{{$extract['type']['extractorName']}}</strong></th>
                </tr>
                <tr>
	            	<th>
	            		<div class="box box-info">
	                    <a>{{$extract['type']['schedule']}} {{$extract['type']['period']}}</a> -
	                    @isset($extract['type']['dayOfWeek'])
   							 <i>Ejecución días {{$extract['type']['dayOfWeek']}}</i>
						@endisset 
						@isset($extract['type']['dayOfMonth'])
   							 <i>Ejecución día {{$extract['type']['dayOfMonth']}} de cada mes</i>
						@endisset 
	                    <i> a las {{$extract['type']['atTime']['hour']}}:{{$extract['type']['atTime']['minute']}} Hrs.</i>
	                  </div>
	            	</th>
                </tr>
              @foreach($extract['extractions'] as $key => $nodesExe)
                @isset($nodesExe['nodes'])
                    
                    @foreach($nodesExe['nodes'] as $key => $nodes)
                        <tr>
                          <td>
                           <strong> Nombre del Nodo:<strong> {{$nodes['nodeName']}}<button type="button" class="btn btn-outline-success btn-icon-text btn-sm" style="float:right;" onclick="downloadFile('{{$nodes['nodeName']}}')">Exportar a CSV  <i class="mdi mdi-export btn-icon-prepend"></i></button>
                          </td>
                        </tr>   
                        @foreach($nodes['data'] as $key => $data)
                              @foreach($data as $key => $text)
                               <tr>
                                  <td>
                                    <div class="box box-info">
                                      <i>{{$key}}: </i><a>{{$text}}</a>
                                    </div>
                                  </td>
                                </tr>   
                              @endforeach
                        @endforeach
                     
                    @endforeach
                 
                @endisset 

                @isset($nodesExe['data'])

                    @foreach($nodesExe['data'] as $key => $nodes)
                    <tr>
                      <td>
                       <strong> Nombre del Nodo:<strong> {{$nodes['nodeName']}}<button type="button" class="btn btn-outline-success btn-icon-text btn-sm" style="float:right;" onclick="downloadFile('{{$nodes['nodeName']}}')">Exportar a CSV  <i class="mdi mdi-export btn-icon-prepend"></i></button>
                      </td>
                    </tr>   
                          @foreach($nodes['data'] as $key => $data)
                                @foreach($data as $key => $text)
                                 <tr>
                                    <td>
                                      <div class="box box-info">
                                        <i>{{$key}}: </i><a>{{$text}}</a>
                                      </div>
                                    </td>
                                  </tr>   
                                @endforeach
                          @endforeach
                         
                    @endforeach
                 
                @endisset 

              @endforeach
              </table> 
            </div>
        </div>
    </div>
   </div>
    <br>
@endforeach     
</div>


  



@endsection
@section('scriptFooter')
<script>
  function arrayObjToCsv(ar) {
  //comprobamos compatibilidad
  if(window.Blob && (window.URL || window.webkitURL)){
    var contenido = "",
      d = new Date(),
      blob,
      reader,
      save,
      clicEvent;
    //creamos contenido del archivo
    for (var i = 0; i < ar.length; i++) {
      //construimos cabecera del csv
      //if (i == 0)
        //contenido += Object.keys(ar[i]).join(";") + "\n";
      //resto del contenido
      contenido += Object.keys(ar[i]).map(function(key){
              return ar[i][key];
            }).join(";") + "\n";
    }
    //creamos el blob
    blob =  new Blob(["\ufeff", contenido], {type: 'text/csv'});
    //creamos el reader
    var reader = new FileReader();
    reader.onload = function (event) {
      //escuchamos su evento load y creamos un enlace en dom
      save = document.createElement('a');
      save.href = event.target.result;
      save.target = '_blank';
      //aquí le damos nombre al archivo
      save.download = "PROGRAMADO_"+ d.getDate() + "_" + (d.getMonth()+1) + "_" + d.getFullYear() +".csv";
      try {
        //creamos un evento click
        clicEvent = new MouseEvent('click', {
          'view': window,
          'bubbles': true,
          'cancelable': true
        });
      } catch (e) {
        //si llega aquí es que probablemente implemente la forma antigua de crear un enlace
        clicEvent = document.createEvent("MouseEvent");
        clicEvent.initEvent('click', true, true);
      }
      //disparamos el evento
      save.dispatchEvent(clicEvent);
      //liberamos el objeto window.URL
      (window.URL || window.webkitURL).revokeObjectURL(save.href);
    }
    //leemos como url
    reader.readAsDataURL(blob);
  }else {
    //el navegador no admite esta opción
    alert("Su navegador no permite esta acción");
  }
};

function downloadFile(nombreNodo) {
  var extracted = <?php echo json_encode($schedule);  ?>;
  //var newEscalafon = JSON.parse(escalafon);
  //console.log(extracted[0]['data']);
  var arrayToImport=[];
  extracted.forEach(function(element) {
    element['extractions'].forEach(function(element2) {

      if (typeof element2['nodes'] !== 'undefined') {
        element2['nodes'].forEach(function(element3) {
          if(element3['nodeName']==nombreNodo){
              element3['data'].forEach(function(element4) {
                arrayToImport.push(element4);
               });
          }
        });
      }

      if (typeof element2['data'] !== 'undefined') {
        element2['data'].forEach(function(element3) {
          if(element3['nodeName']==nombreNodo){
              element3['data'].forEach(function(element4) {
                arrayToImport.push(element4);
               });
          }
        });
      }
      


    });
  });

  arrayObjToCsv(arrayToImport);
}


        $(document).ready(function() {
          $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });

          
          
           $('#exportCSV').on('click', function (e) {
            
            $.ajax({
                  url: '{{Route('exportCSV')}}',
                  type: 'POST',
                  dataType: 'JSON',
                  data: {Extracted},
                  beforeSend: function () {
                    swal({
                      title: 'Espere...',
                      text: 'Exportanto',
                      allowOutsideClick: false,
                      onOpen: () => {
                          swal.showLoading()
                        }
                      }).then((result) => {
                          if (result.dismiss === 'timer') {
                          }
                      })
                      },
                  success: function(result) {
                    swal({
                      type: 'success',
                      title: 'Exportado!',
                      text: 'Bien Hecho',
                      allowOutsideClick: false,
                    }).then(function () {
                          location.reload();
                      })
                  }
                  }).fail(function() {
                      swal(
                          '¡Error!',
                          'Algo a salido mal',
                          'error'
                      )
                  });
                         
           } );
        });



</script>

@endsection