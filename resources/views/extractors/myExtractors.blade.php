@extends('layout.newMain')
@section('content')
<div class="page-header">
  <h3 class="page-title">
    <span class="page-title-icon bg-gradient-info text-white mr-2">
      <i class="mdi mdi-clipboard-outline"></i>                 
    </span>
    Mis Extractores
  </h3>
  <nav aria-label="breadcrumb">
      <ul>
        <button type="button" onclick="location.href='{{route('users.ViewAddExtractor')}}'" class="btn btn-info">Crear Extractor</button>
        <button type="button" onclick="location.href='{{route('users.ViewAddExtractorNew')}}'" class="btn btn-outline-info btn-fw">Crear Extractor Asistido</button>
      </ul>
    </nav>
</div>

  <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <div class="table-responsive">  
              <table id="tableId" class="table table-striped table-sm">
                <thead>
                <tr>
                  <tr>
                      <th class="hidden">extractorId</th>
                      <th>Extractor</th>
                      <th>Descripción</th>
                      <th>URL</th>
                      <th>Ejecutar</th>
                      <th>Programar</th>
                      <th></th>
                  </tr>
                </tr>
                </thead>
            </table>
           </div>
        </div>
      </div>
    </div>

@endsection
@section('scriptFooter')
<script>

        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });
            //console.log(JSON.stringify(collaborators));
            $('#tableId').DataTable( {
                language: {
                    sProcessing:     "Cargando datos...&nbsp;&nbsp;<img height='32' width='32' src='{{url('dist/img/loader.gif')}}'>",
                    sLengthMenu:     "Mostrar _MENU_ registros",
                    sZeroRecords:    "No se encontraron resultados",
                    sEmptyTable:     "No existe ningún registro en este momento",
                    sInfo:           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    sInfoEmpty:      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    sInfoFiltered:   "(filtrado de un total de _MAX_ registros)",
                    sInfoPostFix:    "",
                    sSearch:         "Buscar:",
                    sUrl:            "",
                    sInfoThousands:  ",",
                    sLoadingRecords: "&nbsp;",
                    oPaginate: {
                        sFirst:    "Primero",
                        sLast:     "Último",
                        sNext:     "Siguiente",
                        sPrevious: "Anterior"
                    },
                    oAria: {
                        sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
                        sSortDescending: ": Activar para ordenar la columna de manera descendente"
                    }
                },
                ajax: {
                    url: "{{route('extractors.myextractorsdata')}}",
                    type: "GET",
                    beforeSend: function (request) {
                        request.setRequestHeader("X-CSRF-TOKEN", $('input[name="_token"]').val());
                    },
                },
                    //Employee dates: picture, name and postulating button//
                columns: [
                    { data: "id", class: "hidden", defaultContent: "default" },
                    { data: "name", defaultContent: "default" },
                    { data: "description", defaultContent: "default"  },
                    { data: "url",className: "center",
                      mRender: function (data, type, full) {
                        console.log(data);
                        return '<a href="'+data+'" target="_blank">'+data+'</a>'
                      }
                    },
                    { data: "id", className: "center",
                      mRender: function (data, type, full) {
                        console.log(data);
                        var myUrl = '{{route('extractors.executeExtractor')}}';
                        // myUrl = myUrl.replace(':id:',data);
                        return '<form method="post" action='+`"`+myUrl+`"`+'> {{csrf_field()}} <input class="hidden" type="text" name="id" value='+`"`+data+`"`+'> <input class="hidden" type="text" name="url" value='+`'`+full.url+`'`+'> <button type="submit" class="btn btn-gradient-success btn-rounded btn-icon"><i class="mdi mdi-play-circle btn-icon-prepend"></i></button> </form> '
                      },
                    },
                    {   data: "id", className: "center",
                        mRender: function(data, type, full){
                            return `<div class="btn-group">
                                        <button type="button" class="btn btn-rounded btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                        <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                                          <a class="dropdown-item" onclick="diario('`+data+`')" href="#">Diario</a>
                                          <a class="dropdown-item" onclick="semanal('`+data+`')" href="#">Semanal</a>
                                          <a class="dropdown-item" onclick="mensual('`+data+`')" href="#">Mensual</a>
                                        </div>
                                      </div>`
                        }
                    },
                    {   data: "id", className: "center",
                        mRender: function(data, type, full){
                            return `<div class="btn-group">
                                        <button type="button" class="btn btn-gradient-danger btn-rounded btn-icon"><i class="mdi mdi-delete-forever" onclick="deleteEx('`+data+`')"></i></button>
                                      </div>`
                        }
                    },
                ],
                paging: true,
                lengthChange: true,
                searching: true,
                ordering: true,
                info: true,
                autoWidth: true,
                processing: true,
                //order: [[ 12, "desc" ]],
               
            });
    

            $('#newElement').on('click', function (e) {
                    e.preventDefault();

                    swal({
                        title: 'Añadir Nuevo Extractor',
                        html:
                        '<a>Página Web</a>'+
                        '<input id="swal-input0" class="swal2-input" placeholder="URL">' +
                        '<a>Nombre del Extractor</a>'+
                        '<input id="swal-input1" class="swal2-input" placeholder="Nombre">' +
                        '<a>Descripción</a>'+
                        '<input id="swal-input2" class="swal2-input" placeholder="Nombre">' +
                        '<a>Elemento Contenedor</a>'+
                        '<input id="swal-input3" class="swal2-input" placeholder="Contendor">' +
                        '<a>Nodo del Contenedor</a>'+
                        '<input id="swal-input4" class="swal2-input" placeholder="Nodo">',
                        focusConfirm: false,
                        showCancelButton: true,
                        cancelButtonColor: '#d33',
                        cancelButtonText: 'Cancelar',
                        confirmButtonText: 'Enviar',
                        reverseButtons: true,
                        preConfirm: function () {
                            return new Promise(function (resolve) {
                                resolve([
                                    $('#swal-input1').val(),
                                    $('#swal-input2').val(),
                                    $('#swal-input3').val(),
                                    $('#swal-input4').val()
                                ])
                            })
                        }
                    }).then(function (result) {
                        $.ajax({
                            url: '',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {name:result[0], rate:result[1], sis:result[2], active:"1"},
                            success: function(result) {
                                // Do something with the result
                            }
                        }).done(function( data ) {
                            swal(
                                '¡Enviado!',
                                'Su nuevo registro ha sido almacenado con exito',
                                'success'
                            ).then(function () {
                                location.reload();
                            })
                        }).fail(function(data) {
                            swal(
                                '¡Error!',
                                'El nuevo registro no pudo ser almacenado, favor intente mas tarde',
                                'error'
                            )
                        });
                    }).catch(swal.noop)
                } );

            
        });

        function deleteEx(id) {       
            swal({
                title: 'Eliminar Extractor',
                text:  '¿Realmente deseas eliminar este extractor?',
                type: 'warning',

                focusConfirm: false,
                showCancelButton: true,
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Sí',
                reverseButtons: true,
                
            }).then(function () {
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
                    url: '{{route('users.deleteExtractor')}}',
                    type: 'GET',
                    dataType: 'JSON',
                    data: {extractorID: id},
                    success: function(result) {
                        console.log(result);// Do something with the result
                    }
                }).done(function( data ) {
                    if (data.data.request=="SUCCESSFUL"){
                        swal(
                        '¡Enviado!',
                        'Su nuevo registro ha sido eliminado con exito',
                        'success'
                        ).then(function () {
                            location.reload();
                        })
                    }else{
                        swal(
                        '¡Error!',
                        'El nuevo registro no pudo ser eliminado, favor intente mas tarde',
                        'error'
                        )
                    }
                }).fail(function(data) {
                    swal(
                        '¡Error!',
                        'El nuevo registro no pudo ser eliminado, favor intente mas tarde',
                        'error'
                    )
                });
            }).catch(swal.noop)
        } 

        function diario(id) {       
            swal({
                title: 'Extracción diaria',
                html:
                '<a>Hora de ejecución</a>'+
                '<div><input type="number" id="swal-input0" class="swal2-input" placeholder="Hora" min="0" max="23"> <a> : </a> '+
                '<input type="number" id="swal-input1" class="swal2-input" placeholder="Minutos" min="0" max="59" ></div>',

                focusConfirm: false,
                showCancelButton: true,
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Enviar',
                reverseButtons: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        resolve([
                            $('#swal-input0').val(),
                            $('#swal-input1').val(),
                        ])
                    })
                },
                onOpen: () => {
                    $("#swal-input0").change(function(){
                            if($('#swal-input0').val()<0) {$('#swal-input0').val(0)}
                            if($('#swal-input0').val()>23) {$('#swal-input0').val(23)}
                    });
                    $("#swal-input1").change(function(){
                            if($('#swal-input1').val()<0) {$('#swal-input1').val(0)}
                            if($('#swal-input1').val()>59) {$('#swal-input1').val(59)}
                    });
                }
            }).then(function (result) {
                $.ajax({
                    url: '{{route('schedule.addDailyExtractor')}}',
                    type: 'GET',
                    dataType: 'JSON',
                    data: {extractorID: id, atHour :result[0], atMinute:result[1]},
                    success: function(result) {
                        console.log(result);// Do something with the result
                    }
                }).done(function( data ) {
                    if (data.data.request=="SUCCESSFUL"){
                        swal(
                        '¡Enviado!',
                        'Su nuevo registro ha sido almacenado con exito',
                        'success'
                        ).then(function () {
                            location.reload();
                        })
                    }else{
                        swal(
                        '¡Error!',
                        'El nuevo registro no pudo ser almacenado, favor intente mas tarde',
                        'error'
                        )
                    }
                }).fail(function(data) {
                    swal(
                        '¡Error!',
                        'El nuevo registro no pudo ser almacenado, favor intente mas tarde',
                        'error'
                    )
                });
            }).catch(swal.noop)
        } 

        function semanal(id) {       
            swal({
                title: 'Extracción semanal',
                html:
                '<a>Día de ejecución</a>'+
                `
                  <select class="form-control" id="swal-input0">
                    <option selected value="Lunes">Lunes</option>
                    <option value="Martes">Martes</option>
                    <option value="Miercoles">Miercoles</option>
                    <option value="Jueves">Jueves</option>
                    <option value="Viernes">Viernes</option>
                    <option value="Sabado">Sabado</option>
                    <option value="Domingo">Domingo</option>
                  </select>`+
                '<a>Hora de ejecución</a>'+
                '<div><input type="number" id="swal-input1" class="swal2-input" placeholder="Hora" min="0" max="23"> <a> : </a> '+
                '<input type="number" id="swal-input2" class="swal2-input" placeholder="Minutos" min="0" max="59" ></div>',

                focusConfirm: false,
                showCancelButton: true,
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Enviar',
                reverseButtons: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        resolve([
                            $('#swal-input0').val(),
                            $('#swal-input1').val(),
                            $('#swal-input2').val()
                        ])
                    })
                },
                onOpen: () => {
                    $("#swal-input1").change(function(){
                            if($('#swal-input1').val()<0) {$('#swal-input1').val(0)}
                            if($('#swal-input1').val()>23) {$('#swal-input1').val(23)}
                    });
                    $("#swal-input2").change(function(){
                            if($('#swal-input2').val()<0) {$('#swal-input2').val(0)}
                            if($('#swal-input2').val()>59) {$('#swal-input2').val(59)}
                    });
                }
            }).then(function (result) {
                $.ajax({
                    url: '{{route('schedule.addWeeklyExtractor')}}',
                    type: 'GET',
                    dataType: 'JSON',
                    data: {
                        extractorID: id, 
                        dayOfWeek:result[0], 
                        atHour :result[1], 
                        atMinute:result[2]
                    },
                    success: function(result) {
                       console.log(result); // Do something with the result
                    }
                }).done(function( data ) {
                    if (data.data.request=="SUCCESSFUL"){
                         swal(
                        '¡Enviado!',
                        'Su nuevo registro ha sido almacenado con exito',
                        'success'
                        ).then(function () {
                            location.reload();
                        })
                    }else{
                        swal(
                        '¡Error!',
                        'El nuevo registro no pudo ser almacenado, favor intente mas tarde',
                        'error'
                        )
                    }
                   
                }).fail(function(data) {
                    swal(
                        '¡Error!',
                        'El nuevo registro no pudo ser almacenado, favor intente mas tarde',
                        'error'
                    )
                });
            }).catch(swal.noop)
        } 

        function mensual(id) {       
            swal({
                title: 'Extracción mensual',
                html:
                '<a>Día del mes</a>'+
                '<input type="number" id="swal-input0" class="form-control form-control-lg" placeholder="Dia del mes" min="1" max="31">'+
                '<a>Hora de ejecución</a>'+
                '<div><input type="number" id="swal-input1" class="swal2-input" placeholder="Hora" min="0" max="23"> <a> : </a> '+
                '<input type="number" id="swal-input2" class="swal2-input" placeholder="Minutos" min="0" max="59" ></div>',

                focusConfirm: false,
                showCancelButton: true,
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancelar',
                confirmButtonText: 'Enviar',
                reverseButtons: true,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        resolve([
                            $('#swal-input0').val(),
                            $('#swal-input1').val(),
                            $('#swal-input2').val(),
                        ])
                    })
                },
                onOpen: () => {
                    $("#swal-input1").change(function(){
                            if($('#swal-input1').val()<0) {$('#swal-input1').val(0)}
                            if($('#swal-input1').val()>23) {$('#swal-input1').val(23)}
                    });
                    $("#swal-input2").change(function(){
                            if($('#swal-input2').val()<0) {$('#swal-input2').val(0)}
                            if($('#swal-input2').val()>59) {$('#swal-input2').val(59)}
                    });
                }
            }).then(function (result) {
                $.ajax({
                    url: '{{route('schedule.addMonthlyExtractor')}}',
                    type: 'GET',
                    dataType: 'JSON',
                    data: {
                        extractorID: id, 
                        dayOfMonth: result[0], 
                        atHour :result[1], 
                        atMinute:result[2]
                    },
                    success: function(result) {
                        console.log(result);// Do something with the result
                    }
                }).done(function( data ) {
                    console.log("en done");
                    console.log(data);
                    if (data.data.request=="SUCCESSFUL") {
                        swal(
                        '¡Enviado!',
                        'Su nuevo registro ha sido almacenado con exito',
                        'success'
                        ).then(function () {
                            location.reload();
                        })
                    }else{
                        swal(
                        '¡Error!',
                        'El nuevo registro no pudo ser almacenado, favor intente mas tarde',
                        'error'
                         )
                    }
                    
                }).fail(function(data) {
                    swal(
                        '¡Error!',
                        'El nuevo registro no pudo ser almacenado, favor intente mas tarde',
                        'error'
                    )
                });
            }).catch(swal.noop)
        } 
</script>
@endsection