<!DOCTYPE html>
<html lang="es">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <title>Text Extraction Community</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{ asset('new-style/vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('new-style/vendors/css/vendor.bundle.base.css') }}">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{ asset('new-style/css/style.css') }}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{ asset('new-style/images/somosmini.png') }}">
  <!-- Personal -->
  <link rel="stylesheet" type="text/css" href="{{ asset('dist/css/sweetalert2.min.css') }}">
  
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
  

  <style type="text/css">
    .hidden {
      display: none !important;
    }

	table {
	  font-family: arial, sans-serif;
	  border-collapse: collapse;
	  width: 100%;
	}

	td, th {
	  border: 1px solid #dddddd;
	  text-align: left;
	  padding: 8px;
	}

	tr:nth-child(even) {
	  background-color: #dddddd;
	}

  </style>
</head>

<body>
  <div class="container-scroller">
    <!-- partial:../../partials/_navbar.html -->
    <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo" href="{{route('start.home')}}"><img src="{{ asset('new-style/images/somos.png') }}" alt="logo"/></a>
        <a class="navbar-brand brand-logo-mini"><img src="{{ asset('new-style/images/somosmini.png') }}" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-stretch">
        <ul class="navbar-nav navbar-nav-right">
          <button type="button" onclick="location.href='{{route('login')}}'" class="btn btn-outline-info btn-fw">Ingresar</button>
        </ul>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      
      <!-- partial -->
      <div style="width: 100%" class="main-panel">
        <div class="content-wrapper">
         <div class="page-header">
  <h3 class="page-title">
    <span class="page-title-icon bg-gradient-info text-white mr-2">
      <i class="mdi mdi-meteor"></i>                 
    </span>
    Extractor "{{$extractorName}}"
  </h3>
  <nav aria-label="breadcrumb">
      <ul class="breadcrumb">
        <button type="button" class="btn btn-outline-success btn-icon-text" onclick="location.href='{{route('login')}}'">Si deseas descargar extracciones debes estar registrado<i class="mdi mdi-export btn-icon-prepend"></i></button>
      </ul>
    </nav>
</div>

<div  style="width:50%; height:500px; overflow: auto; float:left;">
@foreach($extractedData as $key => $data)
    <div class="col-xs-12">
        <div class="col-md-12">
            <div class="box box-info">
              <table>
                <tr>
                  <th><strong>{{$data['nodeName']}}</strong></th>
                </tr>
              @foreach($data['data'] as $key => $nodes)
              <tr>
                <td>
                    @foreach($nodes as $key => $text)
                      <div class="box box-info">
                        <i>{{$key}}: </i><a>{{$text}}</a>
                      </div>
                    @endforeach
                  </td>
               </tr>     
              @endforeach
              </table> 
            </div>
        </div>
    </div>
    <br>
@endforeach     
</div>

<div id="miFrame" height="500" align="right" style="width:50%; float:right;">
  
</div>
        </div>
        
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="https://dsi.face.ubiobio.cl/somos/" target="_blank">SOMOS U.B.B</a>. Todos los derechos reservados.</span>
            <!--span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hecho Con <i class="mdi mdi-heart text-danger"></i></span-->
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="{{ asset('new-style/vendors/js/vendor.bundle.base.js') }}"></script>
  <script src="{{ asset('new-style/vendors/js/vendor.bundle.addons.js') }}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="{{ asset('new-style/js/off-canvas.js') }}"></script>
  <script src="{{ asset('new-style/js/misc.js') }}"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <!-- End custom js for this page-->
  <script type="text/javascript" src="{{ asset('dist/js/sweetalert2.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jQuery/jquery-3.1.1.min.js') }}"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>

  <script>
  function arrayObjToCsv(ar) {
  //comprobamos compatibilidad
  if(window.Blob && (window.URL || window.webkitURL)){
    var contenido = "",
      d = new Date(),
      blob,
      reader,
      save,
      clicEvent;
    //creamos contenido del archivo
    for (var i = 0; i < ar.length; i++) {
      //construimos cabecera del csv
      //if (i == 0)
        //contenido += Object.keys(ar[i]).join(";") + "\n";
      //resto del contenido
      contenido += Object.keys(ar[i]).map(function(key){
              return ar[i][key];
            }).join(";") + "\n";
    }
    //creamos el blob
    blob =  new Blob(["\ufeff", contenido], {type: 'text/csv'});
    //creamos el reader
    var reader = new FileReader();
    reader.onload = function (event) {
      //escuchamos su evento load y creamos un enlace en dom
      save = document.createElement('a');
      save.href = event.target.result;
      save.target = '_blank';
      //aquí le damos nombre al archivo
      save.download = "{{$extractorName}}_"+ d.getDate() + "_" + (d.getMonth()+1) + "_" + d.getFullYear() +".csv";
      try {
        //creamos un evento click
        clicEvent = new MouseEvent('click', {
          'view': window,
          'bubbles': true,
          'cancelable': true
        });
      } catch (e) {
        //si llega aquí es que probablemente implemente la forma antigua de crear un enlace
        clicEvent = document.createEvent("MouseEvent");
        clicEvent.initEvent('click', true, true);
      }
      //disparamos el evento
      save.dispatchEvent(clicEvent);
      //liberamos el objeto window.URL
      (window.URL || window.webkitURL).revokeObjectURL(save.href);
    }
    //leemos como url
    reader.readAsDataURL(blob);
  }else {
    //el navegador no admite esta opción
    alert("Su navegador no permite esta acción");
  }
};

function downloadFile() {
  var extracted = <?php echo json_encode($extractedData);  ?>;
  //var newEscalafon = JSON.parse(escalafon);
  //console.log(extracted[0]['data']);
  var arrayToImport=[];
  extracted.forEach(function(element) {
    element['data'].forEach(function(element2) {
      arrayToImport.push(element2);
      console.log(arrayToImport);
     });
  });

  arrayObjToCsv(arrayToImport);
}


        $(document).ready(function() {
          $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });

            (prepareFrame)();

          
           $('#exportCSV').on('click', function (e) {
            
            $.ajax({
                  url: '{{Route('exportCSV')}}',
                  type: 'POST',
                  dataType: 'JSON',
                  data: {Extracted},
                  beforeSend: function () {
                    swal({
                      title: 'Espere...',
                      text: 'Exportanto',
                      allowOutsideClick: false,
                      onOpen: () => {
                          swal.showLoading()
                        }
                      }).then((result) => {
                          if (result.dismiss === 'timer') {
                          }
                      })
                      },
                  success: function(result) {
                    swal({
                      type: 'success',
                      title: 'Exportado!',
                      text: 'Bien Hecho',
                      allowOutsideClick: false,
                    }).then(function () {
                          location.reload();
                      })
                  }
                  }).fail(function() {
                      swal(
                          '¡Error!',
                          'Algo a salido mal',
                          'error'
                      )
                  });
                         
           } );
        });

  function prepareFrame() {
    var ifrm = document.createElement("iframe");
    ifrm.setAttribute("src", "{{$url}}");
    ifrm.style.width = "100%";
    ifrm.style.height = "500px";
    ifrm.style.float = "right";
    $( "#miFrame" ).append(ifrm);
  }

</script>
</body>

</html>
