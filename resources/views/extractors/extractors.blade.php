@extends('layout.newMain')
@section('content')
<div class="page-header">
  <h3 class="page-title">
    <span class="page-title-icon bg-gradient-info text-white mr-2">
      <i class="mdi mdi-clipboard-text"></i>                 
    </span>
    Extractores
  </h3>
</div>

   <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <div class="table-responsive">
                  <table id="tableId" class="table table-striped">
                    <thead>
                      <tr>
                        <tr>
                            <th class="hidden">extractorId</th>
                            <th>Extractor</th>
                            <th>Descripción</th>
                            <th>URL</th>
                            <th>Ejecutar</th>
                        </tr>
                      </tr>
                   
                    </thead>
                  </table>
                  </div>
                </div>
              </div>
            </div>


@endsection
@section('scriptFooter')
<script>
        $(document).ready(function() {
            //console.log(JSON.stringify(collaborators));
            $('#tableId').DataTable( {
                language: {
                    sProcessing:     "Cargando datos...&nbsp;&nbsp;<img height='32' width='32' src='{{url('dist/img/loader.gif')}}'>",
                    sLengthMenu:     "Mostrar _MENU_ registros",
                    sZeroRecords:    "No se encontraron resultados",
                    sEmptyTable:     "No existe ningún registro en este momento",
                    sInfo:           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    sInfoEmpty:      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    sInfoFiltered:   "(filtrado de un total de _MAX_ registros)",
                    sInfoPostFix:    "",
                    sSearch:         "Buscar:",
                    sUrl:            "",
                    sInfoThousands:  ",",
                    sLoadingRecords: "&nbsp;",
                    oPaginate: {
                        sFirst:    "Primero",
                        sLast:     "Último",
                        sNext:     "Siguiente",
                        sPrevious: "Anterior"
                    },
                    oAria: {
                        sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
                        sSortDescending: ": Activar para ordenar la columna de manera descendente"
                    }
                },
                ajax: {
                    url: "{{route('extractors.loadAllExtractors')}}",
                    type: "GET",
                    beforeSend: function (request) {
                        request.setRequestHeader("X-CSRF-TOKEN", $('input[name="_token"]').val());
                    },
                },
                    //Employee dates: picture, name and postulating button//
                columns: [
                    { data: "id", class: "hidden", defaultContent: "default" },
                    { data: "name", defaultContent: "default" },
                    { data: "description", defaultContent: "default"  },
                    { data: "url",className: "center",
                      mRender: function (data, type, full) {
                        console.log(data);
                        return '<a href="'+data+'" target="_blank">'+data+'</a>'
                      }
                    },
                    { data: "id", className: "center",
                      mRender: function (data, type, full) {
                        console.log(data);
                        var myUrl = '{{route('extractors.executeExtractor')}}';
                        // myUrl = myUrl.replace(':id:',data);
                        return '<form method="post" action='+`"`+myUrl+`"`+'> {{csrf_field()}} <input class="hidden" type="text" name="id" value='+`"`+data+`"`+'> <input class="hidden" type="text" name="url" value='+`'`+full.url+`'`+'> <button type="submit" class="btn btn-gradient-success btn-rounded btn-icon"><i class="mdi mdi-play-circle btn-icon-prepend"></i></button> </form> '
                      }
                    },
                ],
                paging: true,
                lengthChange: true,
                searching: true,
                ordering: true,
                info: true,
                autoWidth: true,
                processing: true,
                //order: [[ 12, "desc" ]],
               
            });

            
        });
</script>
@endsection