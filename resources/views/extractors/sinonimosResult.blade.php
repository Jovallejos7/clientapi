@extends('layout.newMain')
@section('content')
 <style type="text/css">
    .card .card-body {
        padding: 2.5rem 2.5rem;
        background-color: gainsboro !important;
    }
  </style>

<div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-info text-white mr-2">
        <i class="mdi mdi-arrange-send-to-back"></i>                 
      </span>
      Buscar Sinónimos
    </h3>
</div>

<div class="col-lg-12 grid-margin">
  <div class="card">
    <div class="card-body">  
       <label for="web">Resultados:  </label> 
       <nav aria-label="breadcrumb">
              <ol class="breadcrumb">

                <li class="breadcrumb-item"><button type="button" class="btn btn-outline-success btn-icon-text" onclick="downloadFile('Sinónimos','.txt')">Descargar en TXT  <i class="mdi mdi-export btn-icon-prepend"></i></button><button type="button" class="btn btn-outline-success btn-icon-text" onclick="downloadFile('Sinónimos','.csv')">Descargar en CSV  <i class="mdi mdi-export btn-icon-prepend"></i></button></li>
              </ol>
            </nav>
       <textarea id="TextArea1" rows="50" cols="50" name="palabras" class="form-control" form="myForm" disabled>{{$text}}</textarea>
    </div>
  </div>
</div>
@endsection
@section('scriptFooter')
<script>
function downloadFile(name, format) {
  //comprobamos compatibilidad
  if(window.Blob && (window.URL || window.webkitURL)){
    var contenido = "",
      d = new Date(),
      blob,
      reader,
      save,
      clicEvent;
    //creamos contenido del archivo
    contenido=document.getElementById("TextArea1").value;

    //Saltos de linea compatibles con windows
    contenido=contenido.replace(/\n/g, "\r\n");
    
    blob =  new Blob(["\ufeff", contenido], {type: 'text/csv'});
    //creamos el reader
    var reader = new FileReader();
    reader.onload = function (event) {
      //escuchamos su evento load y creamos un enlace en dom
      save = document.createElement('a');
      save.href = event.target.result;
      save.target = '_blank';
      //aquí le damos nombre al archivo
      save.download = name+"_"+d.getDate() + "_" + (d.getMonth()+1) + "_" + d.getFullYear() +format;
      try {
        //creamos un evento click
        clicEvent = new MouseEvent('click', {
          'view': window,
          'bubbles': true,
          'cancelable': true
        });
      } catch (e) {
        //si llega aquí es que probablemente implemente la forma antigua de crear un enlace
        clicEvent = document.createEvent("MouseEvent");
        clicEvent.initEvent('click', true, true);
      }
      //disparamos el evento
      save.dispatchEvent(clicEvent);
      //liberamos el objeto window.URL
      (window.URL || window.webkitURL).revokeObjectURL(save.href);
    }
    //leemos como url
    reader.readAsDataURL(blob);
  }else {
    //el navegador no admite esta opción
    alert("Su navegador no permite esta acción");
  }
};

</script>
<script>
  $(document).ready(function() {
          $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });

          swal({
            title: 'Buscando Sinónimos...',
            text: 'Esto puede tardar varios minutos',
            allowOutsideClick: false,
            onOpen: () => {
                swal.showLoading()
                var buscar = {!! json_encode($buscar) !!};
                var palabras = {!! json_encode($palabras) !!};
                buscar.forEach(function callback(currentValue, index) {
                    //iterador
                    $.ajax({
                      headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
                      url: '{{route('sinonimosPalabra')}}',
                      type: 'POST',
                      dataType: 'JSON',
                      data: {buscar:currentValue,
                              palabra:palabras[index]},
                      success: function(result) {
                          // Do something with the result
                      },
                      beforeSend: function (request) {

                      },
                    }).done(function( data ) {
                        var porcentaje =  parseInt(((index+1)*100) / buscar.length);
                        var completas = index+1;
                        document.getElementById("swal2-content").innerText = completas+" palabras completadas de "+buscar.length+", ("+ porcentaje+"%)";
                        document.getElementById("TextArea1").value=document.getElementById("TextArea1").value + data.text;

                        if(index == buscar.length-1){
                          swal({
                              title: 'Completado',
                              text: '100%',
                              showConfirmButton: true,
                              allowOutsideClick: false,
                          })
                        }
                    }).fail(function(data) {
                        console.log(data);
                        swal(
                            '¡Error!',
                            'favor intente mas tarde',
                            'error'
                        )
                    });
                });
              },
            }).then((result) => {
                if (result.dismiss === 'timer') {
                }
            })
          
          });
</script>
@endsection