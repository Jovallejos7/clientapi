@extends('layout.newMain')
@section('content')
 <style type="text/css">
    .card .card-body {
        padding: 2.5rem 2.5rem;
        background-color: gainsboro !important;
    }
  </style>

<div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-info text-white mr-2">
        <i class="mdi mdi-arrange-send-to-back"></i>                 
      </span>
      Buscar Sinónimos
    </h3>
</div>



<div class="col-lg-12 grid-margin">
  <div class="card">
    <div class="card-body">
      <form method="POST" name="myForm" id="myForm" action="{{route('sinonimosExtractor')}}">
        {{ csrf_field() }}
        <div class="form-group">
            <div class="form-group">
                <label for="web">Palabras:</label>
                <textarea rows="50" cols="50" name="palabras" class="form-control" form="myForm" required></textarea>
            </div>
        </div>
      <button id="send" type="submit" class="btn btn-info">ENVIAR</button>
    </form>
    </div>
  </div>
</div>

@endsection
@section('scriptFooter')
<script>
$('#send').on('click', function (e){
    swal({
    title: 'Buscando Sinónimos...',
    text: 'Esto puede tardar varios minutos',
    allowOutsideClick: false,
    onOpen: () => {
        swal.showLoading()
      }
    }).then((result) => {
        if (result.dismiss === 'timer') {
        }
    })
 });
</script>
@endsection