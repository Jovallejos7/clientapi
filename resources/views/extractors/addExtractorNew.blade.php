@extends('layout.newMain')
@section('content')
 <style type="text/css">
    .card .card-body {
        padding: 2.5rem 2.5rem;
        background-color: gainsboro !important;
    }
  </style>
<div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-info text-white mr-2">
        <i class="mdi mdi-plus-circle"></i>                 
      </span>
      Añadir Nuevo Extractor
    </h3>
</div>



<div class="col-lg-12 grid-margin">
    <div  style="width:50%; height:500px; overflow: auto; float:left;">
              <div class="card">
                <div class="card-body">
                  <form method="post" name="myForm" id="myForm" action="{{route('users.addExtractor')}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="form-group">
                            <label for="web">Página Web : <button onclick="prepareFrame($('#web').val())" type="button" class="btn btn-gradient-info btn-fw">Ver Página</button></label>
                            <input type="url" class="form-control" id="web" name="url" placeholder="http://wwww.mipagina.cl/" required>
                        </div>
                        <div class="form-group">
                            <label for="nombre">Nombre :</label>
                            <input type="text" min="8" class="form-control" id="nombre" name="name" placeholder="Mi Extractor" required>
                        </div>
                        <div class="form-group">
                            <label for="Descripción">Descripción:</label>
                            <input type="text" min="8" class="form-control" id="Descripción" name="description" placeholder="Esto es un extractor" required>
                        </div>
                        <div class="form-group form-check">
                            <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="shared" > Hacer Publico
                            </label>
                        </div>
                        <button type="button" onclick="nodos()" class="btn btn-gradient-info btn-rounded btn-fw">Buscar Contenedores</button>   
                    </div>

                    
                    <div id="nodes">
                        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        </div>
                        <div id="nodes-1">
                        <h3>Nodo 1  <button type="button" class="btn btn-inverse-info btn-icon" data-toggle="tooltip" data-placement="top" title="Capaces de ingresar y capturar elementos de sección específica de una página web a través de un Contenedor y Elementos."><i class="mdi mdi-help"></i></button></h3>
                        <div class="form-group">
                            <label for="nombre">Nombre :</label>
                            <input type="text" class="form-control" name="name-1" placeholder="Mi Nodo">
                        </div>


                        <div class="form-group">
                            <label for="contenedor">Contenedor:  <button type="button" class="btn btn-inverse-info btn-icon" data-toggle="tooltip" data-placement="top" title="Has click en 'Buscar contendores' para completar este campo. Es un elemento o estructura que generalmente se repite en las páginas web para describir o identificar elementos concurrentes direccionando al Nodo hacia los elementos que contienen el texto deseado para la extracción."><i class="mdi mdi-help"></i></button></label>
                            <div id="dinamicNodo"></div>
                        </div>
                        <div class="form-group">
                            <label for="descripción">Descripción:</label>
                            <input type="text" class="form-control" name="description-1" placeholder="Esto es un nodo">
                        </div>
                        <button type="button" onclick="elementos()" class="btn btn-gradient-info btn-rounded btn-fw">Buscar Selector</button>
                        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        </div>
                        <div id="elements-node-1">
                        <div id="elements-1">
                            <h3>Elemento 1 - Nodo 1 <button type="button" class="btn btn-inverse-info btn-icon" data-toggle="tooltip" data-placement="top" title="Elementos capaces de seleccionar o capturar elementos almacenados en el Contenedor, otorgando al extractor la última información necesaria para extraer el texto deseado."><i class="mdi mdi-help"></i></button></h3>
                            <div class="form-group">
                                <label for="etiqueta">Etiqueta :</label>
                                <input type="text" class="form-control" name="tag-1-1" placeholder="miElemento">
                            </div>
                            <div class="form-group">
                                <label for="selector">Selector :  <button type="button" class="btn btn-inverse-info btn-icon" data-toggle="tooltip" data-placement="top" title="Has click en 'Buscar selector' para completar este campo. Es el elemento final en la costrucción del nodo, corresponde al elemento que otorga el texto que sera extraido."><i class="mdi mdi-help"></i></button></label>
                                <div id="dinamicElement"></div>
                            </div>
                            
                        </div>
                    </div>
                    <!--button type="button" onclick="newElement(1)" class="btn btn-success">Nuevo Elemento</button-->   
                    </div>
                    
                    </div>
                    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom"></div>
                    <!--button type="button" id="newNode" class="btn btn-success">Nuevo Nodo</button-->
                  
                  <button type="submit" class="btn btn-info">Guardar</button>
                </form>
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom"></div>
                <button type="button" class="btn btn-success btn-fw test" onClick="getFormData();">Probar</button>
                </div>
              </div>
              <!--button onclick="newExtract()" class="btn btn-info">ver array</button-->
            </div>
            <div id="miFrame" height="500" align="right" style="width:50%; float:right;">
        </div>



@endsection
@section('scriptFooter')
<script>

    function prepareFrame(url) {
        $( "#miFrame" ).empty();
        var ifrm = document.createElement("iframe");
        ifrm.setAttribute("src", url);
        ifrm.style.width = "100%";
        ifrm.style.height = "500px";
        ifrm.style.float = "right";
        $( "#miFrame" ).append(ifrm);
    }


	var nodesCounter=1;
    var elementInNode=[1];
    $("select[name=container-1]").change(function(){
        console.log("holaaaaaa");
            $( "#dinamicElement").empty();
    });

   function getFormData(){
       var config = {};
        $('input').each(function () {
         config[this.name] = this.value;
        });
        $('select').each(function () {
         config[this.name] = this.value;
        });

        console.log(config);

         $.ajax({
                headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
                url: '{{route('extractors.TestExtractor')}}',
                type: 'POST',
                dataType: 'JSON',
                data: config,
                success: function(result) {
                    // Do something with the result
                },
                beforeSend: function (request) {
                  swal({
                    title: 'Espere...!',
                    text: 'Ejecutando Test',
                    onOpen: () => {
                      swal.showLoading()
                    }
                  }).then((result) => {
                    if (result.dismiss === 'timer') {
                    }
                  })
                },
            }).done(function( data ) {
                console.log(data);
                var nombre="";
                console.log(data.data.request);
                if (data.data.request=="SUCCESSFUL") {
                    var nombre = data.data.data.extractorName;
                    var html="";
                    $.each(data.data.data.extractedData, function(key, value){
                        var table =  '<div class="col-xs-12"> <div class="col-md-12"> <div class="box box-info">'+
                                     '<table class="table-striped"><tr>'+ 
                                        '<th><strong>'+value.nodeName+'</strong></th>'+
                                    '</tr>';
                        var fila="<tr><td>";
                       
                            
                            $.each(value.data, function(key2, elemento){
                                 var nodes="";
                                $.each(elemento, function(index, text){
                                    nodes = nodes+'<div class="box box-info">'+
                                                '<i><strong>'+index+': </strong></i><a>'+text+'</a>'+
                                              '</div>'
                                });
                                fila=fila+nodes;
                                fila=fila+"<tr><td>";
                            });

                       
                        table=table+fila+'</table>';
                        html=html+table+'</div></div></div>';
                    });


                    swal({
                        title: nombre,
                        html: html,
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Confirmar',
                        cancelButtonText: 'Cancelar',
                        confirmButtonClass: 'btn btn-gradient-info btn-fw',
                        cancelButtonClass: 'btn btn-gradient-danger btn-fw',
                        onOpen: function() {
                        },
                        preConfirm: function () {
                         
                        }
                      }).then(() => {
                      
                      });


                }else{
                    swal(
                        '¡Error!',
                        'favor intente mas tarde',
                        'error'
                    )
                }
                

            }).fail(function(data) {
                console.log(data);
                swal(
                    '¡Error!',
                    'favor intente mas tarde',
                    'error'
                )
            });
        
    }

    function nodos(){
        $( "#dinamicNodo" ).empty();
        $( "#dinamicElement" ).empty();
        var url= $('#web').val();
        console.log(url);
        $.ajax({
                headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
                url: '{{route('users.getNodes')}}',
                type: 'GET',
                dataType: 'JSON',
                data: {url:url},
                success: function(result) {
                    // Do something with the result
                },
                beforeSend: function (request) {
                  swal({
                    title: 'Espere...!',
                    text: 'Buscando contenedores',
                    onOpen: () => {
                      swal.showLoading()
                    }
                  }).then((result) => {
                    if (result.dismiss === 'timer') {
                    }
                  })
                },
            }).done(function( data ) {
                swal.close();
                console.log(data);

                var options='<option value="" selected>Seleccione un nodo...</option>';
                    data.data.forEach(function(node) {
                      options = options+'<option value="'+node+'">'+node+'</option>'
                    });

                var selectedNodos='<select class="form-control" id="nodo-d-1" name="container-1">'+
                           options+
                        '</select><br>';

                $( "#dinamicNodo" ).append(selectedNodos);
                $("select[name=container-1]").change(function(){
                    console.log("holaaaaaa");
                        $( "#dinamicElement").empty();
                });

            }).fail(function(data) {
                console.log(data);
                swal(
                    '¡Error!',
                    'favor intente mas tarde',
                    'error'
                )
            });
    }

    function elementos(){
        $( "#dinamicElement" ).empty();
        var url= $('#web').val();
        console.log(url);
        $.ajax({
                headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
                url: '{{route('users.getElements')}}',
                type: 'GET',
                dataType: 'JSON',
                data: { url:url,
                        nodo: $('#nodo-d-1').val()
                    },
                success: function(result) {
                    // Do something with the result
                },
                beforeSend: function (request) {
                  swal({
                    title: 'Espere...!',
                    text: 'Buscando Elementos',
                    onOpen: () => {
                      swal.showLoading()
                    }
                  }).then((result) => {
                    if (result.dismiss === 'timer') {
                    }
                  })
                },
            }).done(function( data ) {
                swal.close();
                console.log(data);

                var options='<option value="" selected>Seleccione un nodo...</option>';
                    data.data.forEach(function(element) {
                      options = options+'<option value="'+element+'">'+element+'</option>'
                    });

                var selectedNodos='<select class="form-control" id="elemnts-d-1" name="selector-1-1">'+
                           options+
                        '</select><br>';

                $( "#dinamicElement" ).append(selectedNodos);

            }).fail(function(data) {
                console.log(data);
                swal(
                    '¡Error!',
                    'favor intente mas tarde',
                    'error'
                )
            });
    }
        	
        	function newElement($node){
                if (elementInNode[$node-1]<10) {
                    elementInNode[$node-1]+= 1;
                    $( "#elements-node-"+$node+"" ).append(
                        '<div id="elements-'+elementInNode[$node-1]+'"> <h3>Elemento '+elementInNode[$node-1]+' - Nodo '+$node+'</h3> <div class="form-group"> <label for="etiqueta">Etiqueta :</label> <input type="text" class="form-control" name="tag-'+elementInNode[$node-1]+'-'+$node+'" placeholder="miElemento"> </div> <div class="form-group"> <label for="selector">Selector :</label> <input type="text" class="form-control" name="selector-'+elementInNode[$node-1]+'-'+$node+'" placeholder="a"> </div> </div>'); 
                    }
        		
            }

        	$("#newNode").click(function() {
                if (nodesCounter<10) {
                    nodesCounter += 1;
                    elementInNode.push(1);
                    $( "#nodes" ).append(
                        '<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom"> </div> <div id="nodes-'+nodesCounter+'"> <h3>Nodo '+nodesCounter+'</h3> <div class="form-group"> <label for="nombre">Nombre :</label> <input type="text" class="form-control" name="name-'+nodesCounter+'" placeholder="Mi Nodo"> </div> <div class="form-group"> <label for="contenedor">Contenedor:</label> <input type="text" class="form-control" name="container-'+nodesCounter+'" placeholder="div #elementID .miClass"> </div> <div class="form-group"> <label for="descripción">Descripción:</label> <input type="text" class="form-control" name="description-'+nodesCounter+'" placeholder="Esto es un nodo"> </div> <div id="elements-node-'+nodesCounter+'"> <div id="elements-'+nodesCounter+'"> <h3>Elemento 1 - Nodo '+nodesCounter+'</h3> <div class="form-group"> <label for="etiqueta">Etiqueta :</label> <input type="text" class="form-control" name="tag-1-'+nodesCounter+'" placeholder="miElemento"> </div> <div class="form-group"> <label for="selector">Selector :</label> <input type="text" class="form-control" name="selector-1-'+nodesCounter+'" placeholder="a"> </div> </div> </div> <button type="button" onclick="newElement('+nodesCounter+')" class="btn btn-success">Nuevo Elemento</button> </div>'); 

                    }
	            
	        });

        $(document).ready(function() {

            (prepareFrame)("{{Route('error.noPage')}}");

            $("#nodo-d-1").change(function(){
                    $( "#dinamicElement" ).empty();
            });
        	
      
            $('#newElement').on('click', function (e) {
                    e.preventDefault();

                    swal({
                        title: 'Añadir Nuevo Extractor',
                        html:
                        '<a>Página Web</a>'+
                        '<input id="swal-input0" class="swal2-input" placeholder="URL">' +
                        '<a>Nombre del Extractor</a>'+
                        '<input id="swal-input1" class="swal2-input" placeholder="Nombre">' +
                        '<a>Descripción</a>'+
                        '<input id="swal-input2" class="swal2-input" placeholder="Nombre">' +
                        '<a>Elemento Contenedor</a>'+
                        '<input id="swal-input3" class="swal2-input" placeholder="Contendor">' +
                        '<a>Nodo del Contenedor</a>'+
                        '<input id="swal-input4" class="swal2-input" placeholder="Nodo">',
                        focusConfirm: false,
                        showCancelButton: true,
                        cancelButtonColor: '#d33',
                        cancelButtonText: 'Cancelar',
                        confirmButtonText: 'Enviar',
                        reverseButtons: true,
                        preConfirm: function () {
                            return new Promise(function (resolve) {
                                resolve([
                                    $('#swal-input1').val(),
                                    $('#swal-input2').val(),
                                    $('#swal-input3').val(),
                                    $('#swal-input4').val()
                                ])
                            })
                        }
                    }).then(function (result) {
                        $.ajax({
                            url: '',
                            type: 'POST',
                            dataType: 'JSON',
                            data: {name:result[0], rate:result[1], sis:result[2], active:"1"},
                            success: function(result) {
                                // Do something with the result
                            }
                        }).done(function( data ) {
                            swal(
                                '¡Enviado!',
                                'Su nuevo registro ha sido almacenado con exito',
                                'success'
                            ).then(function () {
                                location.reload();
                            })
                        }).fail(function(data) {
                            swal(
                                '¡Error!',
                                'El nuevo registro no pudo ser almacenado, favor intente mas tarde',
                                'error'
                            )
                        });
                    }).catch(swal.noop)
                } );

            
        });
</script>
@endsection