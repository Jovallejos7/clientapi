@extends('layout.newMain')
@section('content')

<style>
table {
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>

<div class="page-header">
  <h3 class="page-title" style="color:#1da1f2">
    <span class="page-title-icon bg-gradient-info text-white mr-2">
      <i class="mdi mdi-twitter"></i>                 
    </span>
    Twitter
  </h3>
  <nav aria-label="breadcrumb">
      <ul class="breadcrumb">
        <button type="button" class="btn btn-outline-success btn-icon-text" onclick="downloadFile()">Exportar a CSV  <i class="mdi mdi-export btn-icon-prepend"></i></button>
      </ul>
    </nav>
</div>

<div class="col-lg-12 grid-margin stretch-card" style="overflow: auto;">
      <div class="card">
        <div class="card-body">
          <div class="form-group">
            
            <div class="input-group">
              <input class="form-control camel-case" id="query" placeholder="Escriba Aqui Su Consulta" name="query" type="text" onkeyup = "if(event.keyCode == 13) newTable(this.value)">
              <div class="input-group-append">
                <button class="btn btn-sm btn-gradient-info" onclick="newTable($('#query').val())" type="button">Buscar en Twitter</button>
              </div>
                <button class="btn btn-sm btn-gradient-light" onclick="operadores()" type="button">Operadores</button>
            </div>
          </div>
          <div class="table-responsive">  
              <table id="tableId" class="table table-striped table-sm">
                <table id="tableId" class="table table-striped table-sm">
                  <thead>
                  <tr>
                    <tr>
                        <th>Resultados</th>
                    </tr>
                  </tr>
                  </thead>
              </table>
           </div>
        </div>
      </div>
    </div>

@endsection

@section('scriptFooter')
<script type="text/javascript">


          var arrayToImport=[];
          var myQuery="";

          function cargarQuery($query){
              $('#query').val($query);
          }

          function operadores(){

            swal({
                title: 'Operadores',
                html:
                `<div class="input-group">
                  <input class="form-control camel-case" id="ope1" value="hola mundo" readonly type="text">
                  <div class="input-group-append">
                    <button class="btn copy btn-outline-info btn-fw" value="hola mundo" type="button">Utilizar Ejemplo</button>
                  </div>
                </div><h6>Otorga Tweets que contengan "hola" y "mundo".</h6>
                <br>

                <div class="input-group">
                  <input class="form-control camel-case" id="ope2" value='"hola mundo"' readonly type="text">
                  <div class="input-group-append">
                    <button class="btn copy btn-outline-info btn-fw" value='"hola mundo"' type="button">Utilizar Ejemplo</button>
                  </div>
                </div><h6>Otorga Tweets que contengan exactamente "hola mundo".</h6>
                <br>

                <div class="input-group">
                  <input class="form-control camel-case" id="ope3" value="amor OR odio" readonly type="text">
                  <div class="input-group-append">
                    <button class="btn copy btn-outline-info btn-fw" value="amor OR odio" type="button">Utilizar Ejemplo</button>
                  </div>
                </div><h6>Otorga Tweets que contengan "amor" u "odio" o ambos.</h6>
                <br>

                <div class="input-group">
                  <input class="form-control camel-case" id="ope4" value="cerveza -raíz" readonly type="text">
                  <div class="input-group-append">
                    <button class="btn copy btn-outline-info btn-fw" value="cerveza -raíz" type="button">Utilizar Ejemplo</button>
                  </div>
                </div><h6>Otorga Tweets que contengan "cerveza" pero no "raíz".</h6>
                <br>

                <div class="input-group">
                  <input class="form-control camel-case" id="ope5" value="#medidoresinteligentes"  readonly type="text">
                  <div class="input-group-append">
                    <button class="btn copy btn-outline-info btn-fw" value="#medidoresinteligentes" type="button">Utilizar Ejemplo</button>
                  </div>
                </div><h6>Otorga Tweets conteniendo el hashtag "medidoresinteligentes".</h6>
                <br>

                <div class="input-group">
                  <input class="form-control camel-case" id="ope6" value="from: alexiskold" readonly type="text">
                  <div class="input-group-append">
                    <button class="btn copy btn-outline-info btn-fw" value="from: alexiskold" type="button">Utilizar Ejemplo</button>
                  </div>
                </div><h6>Otorga Tweets enviados por "alexiskold".</h6>
                <br>

                <div class="input-group">
                  <input class="form-control camel-case" id="ope7" value="to:techcrunch" readonly type="text">
                  <div class="input-group-append">
                    <button class="btn copy btn-outline-info btn-fw" value="to:techcrunch" type="button">Utilizar Ejemplo</button>
                  </div>
                </div><h6>Otorga Tweets enviados a "techcrunch".</h6>
                <br>

                <div class="input-group">
                  <input class="form-control camel-case" id="ope8" value="@mashable" readonly type="text">
                  <div class="input-group-append">
                    <button class="btn copy btn-outline-info btn-fw" value="@mashable" type="button">Utilizar Ejemplo</button>
                  </div>
                </div><h6>Otorga Tweets que hagan referencia a "mashable".</h6>
                <br>

                <div class="input-group">
                  <input class="form-control camel-case" id="ope9" value='"hora feliz" near:"san francisco"' placeholder="TECO API" readonly type="text">
                  <div class="input-group-append">
                    <button class="btn copy btn-outline-info btn-fw" value='"hora feliz" near:"san francisco"' type="button">Utilizar Ejemplo</button>
                  </div>
                </div><h6>Otorga Tweets que contengan exactamente "hora feliz" y sean enviados cerca de "san francisco".</h6>
                <br>

                <div class="input-group">
                  <input class="form-control camel-case" id="ope10" value="near:NYC within:15mi" readonly type="text">
                  <div class="input-group-append">
                    <button class="btn copy btn-outline-info btn-fw" value="near:NYC within:15mi" type="button">Utilizar Ejemplo</button>
                  </div>
                </div><h6>Otorga Tweets enviados a 15 millas de "la ciudad de NY".</h6>
                <br>

                <div class="input-group">
                  <input class="form-control camel-case" id="ope11" value="superhero since:2010-12-27"  readonly type="text">
                  <div class="input-group-append">
                    <button class="btn copy btn-outline-info btn-fw" value="superhero since:2010-12-27" type="button">Utilizar Ejemplo</button>
                  </div>
                </div><h6>Otorga Tweets que contengan "superhéroe" y sean enviados desde la fecha "2010-12-27" (año-mes-día).</h6>
                <br>

                <div class="input-group">
                  <input class="form-control camel-case" id="ope12" value="ftw until:2010-12-27"  readonly type="text">
                  <div class="input-group-append">
                    <button class="btn copy btn-outline-info btn-fw" value="ftw until:2010-12-27" type="button">Utilizar Ejemplo</button>
                  </div>
                </div><h6>Otorga Tweets que contengan "ftw" y sean enviados hasta la fecha "2010-12-27".</h6>
                <br>

                <div class="input-group">
                  <input class="form-control camel-case" id="ope13" value="gracioso filter:links" readonly type="text">
                  <div class="input-group-append">
                    <button class="btn copy btn-outline-info btn-fw" value="gracioso filter:links" type="button">Utilizar Ejemplo</button>
                  </div>
                </div><h6>Otorga Tweets que contengan "gracioso" y enlacen a las URL.</h6>
                `,
                focusConfirm: false,
                reverseButtons: true,
                onOpen: () => {
                    $('.copy').on('click', function() {
                      console.log("holi");
                      $('#query').val($(this).val());
                      swal(
                        'Ejemplo copiado!',
                        'Se a copiado el ejemplo: '+$(this).val(),
                        'success'
                        )
                    });
                }
            }).then(function () {
               console.log("hola")
            }).catch(swal.noop)

          }

          function newTable($query){
            $('#tableId').DataTable().destroy();
            myQuery=$query;
            loadTable($query);
          }

          function downloadFile() {
              arrayObjToCsv(arrayToImport);
            }

            function arrayObjToCsv(ar) {
              //comprobamos compatibilidad
              if(window.Blob && (window.URL || window.webkitURL)){
                var contenido = "",
                  d = new Date(),
                  blob,
                  reader,
                  save,
                  clicEvent;
                //creamos contenido del archivo
                for (var i = 0; i < ar.length; i++) {
                  //construimos cabecera del csv
                  if (i == 0)
                    contenido += Object.keys(ar[i]).join(";") + "\n";
                  //resto del contenido
                  contenido += Object.keys(ar[i]).map(function(key){

                          return ar[i][key].toString().split("\n").join("");
                        }).join(";") + "\n";
                }

                //creamos el blob
                blob =  new Blob(["\ufeff", contenido], {type: 'text/csv'});
                //creamos el reader
                var reader = new FileReader();
                reader.onload = function (event) {
                  //escuchamos su evento load y creamos un enlace en dom
                  save = document.createElement('a');
                  save.href = event.target.result;
                  save.target = '_blank';
                  //aquí le damos nombre al archivo
                  save.download = myQuery+"_"+ d.getDate() + "_" + (d.getMonth()+1) + "_" + d.getFullYear() +".csv";
                  try {
                    //creamos un evento click
                    clicEvent = new MouseEvent('click', {
                      'view': window,
                      'bubbles': true,
                      'cancelable': true
                    });
                  } catch (e) {
                    //si llega aquí es que probablemente implemente la forma antigua de crear un enlace
                    clicEvent = document.createEvent("MouseEvent");
                    clicEvent.initEvent('click', true, true);
                  }
                  //disparamos el evento
                  save.dispatchEvent(clicEvent);
                  //liberamos el objeto window.URL
                  (window.URL || window.webkitURL).revokeObjectURL(save.href);
                }
                //leemos como url
                reader.readAsDataURL(blob);
              }else {
                //el navegador no admite esta opción
                alert("Su navegador no permite esta acción");
              }
            }

          function loadTable($query){
            console.log($query);
            if($query==""){
              var url = '{{route('extractors.executeTwitterExtractorNull')}}';
            }
            else{
              if($query.indexOf('#') != -1){
              $query =  $query.replace('#', "hashtag:");
              }
              var url = '{{route('extractors.executeTwitterExtractor', ['query'=>':query:'])}}';
               url = url.replace(':query:', $query);
            }

            

            
            console.log(url);
            $('#tableId').DataTable( {
                language: {
                    sProcessing:     "Cargando datos...&nbsp;&nbsp;<img height='32' width='32' src='{{url('dist/img/loader.gif')}}'>",
                    sLengthMenu:     "Mostrar _MENU_ registros",
                    sZeroRecords:    "No se encontraron resultados",
                    sEmptyTable:     "No se a ejecutado ninguna consulta aún",
                    sInfo:           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    sInfoEmpty:      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    sInfoFiltered:   "(filtrado de un total de _MAX_ registros)",
                    sInfoPostFix:    "",
                    sSearch:         "Buscar en los resultados:",
                    sUrl:            "",
                    sInfoThousands:  ",",
                    sLoadingRecords: "&nbsp;",
                    oPaginate: {
                        sFirst:    "Primero",
                        sLast:     "Último",
                        sNext:     "Siguiente",
                        sPrevious: "Anterior"
                    },
                    oAria: {
                        sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
                        sSortDescending: ": Activar para ordenar la columna de manera descendente"
                    }
                },
                ajax: {
                    url: url,
                    type: "GET",
                    beforeSend: function (request) {
                        request.setRequestHeader("X-CSRF-TOKEN", $('input[name="_token"]').val());
                    },
                },
                    //Employee dates: picture, name and postulating button//
                columns: [
                    { data: null,className: "center",
                      mRender: function (data, type, full) {
                        console.log(data);
                        var answerto = "";
                        var text = data.text;
                        var users = data.user.split("@").filter(Boolean);
                        users.forEach(function(element) {
                          if(users[0]!=element){
                            answerto = answerto+'<a href="https://twitter.com/'+element+'" target="_blank"> @'+element+'</a>'
                            console.log(element);
                          }
                        });

                        if(answerto!=""){
                          answerto = '<tr> <td><em>en respuesta a</em>'+answerto+'</td> </tr>';
                        }
                        
                        text.replace("\n", "<br>");
                        return '<table> <tr><td><a href="https://twitter.com/'+users[0]+'" target="_blank"><strong>'+data.name+' </strong></a><em> @'+users[0]+'</em> . <em>'+data.date+'</em></td> </tr> '+answerto+' <tr> <td>'+text+'</td> </tr> <tr> <td><a><i class="mdi mdi-message-outline"></i><span>'+data.replys+'   </span></a><a><i class="mdi mdi-twitter-retweet"></i><span>'+data.retweet+'   </span></a><a><i class="mdi mdi-heart-outline"></i><span>'+data.favorite+'   </span></a></td> </tr> </table>'} },
                ],
                paging: true,
                lengthChange: true,
                searching: true,
                ordering: true,
                info: true,
                autoWidth: true,
                processing: true,
                //order: [[ 12, "desc" ]],
               "initComplete": function(settings, json) {
                  arrayToImport = json['data'];
                }
            });
          }
  
</script>
<script>
  $(document).ready(function() {
    var nullQuery="{{$query}}";
    (loadTable)(nullQuery);
  });
</script>
@endsection