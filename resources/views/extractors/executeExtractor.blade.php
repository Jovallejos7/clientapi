@extends('layout.newMain')
@section('content')
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
<div class="page-header">
  <h3 class="page-title">
    <span class="page-title-icon bg-gradient-info text-white mr-2">
      <i class="mdi mdi-meteor"></i>                 
    </span>
    Extractor "{{$extractorName}}"
  </h3>
</div>

<div  style="width:50%; height:500px; overflow: auto; float:left;">
@foreach($extractedData as $key => $data)
    <div class="col-xs-12">
        <div class="col-md-12">
            <div class="box box-info">
              <table>
                <tr>
                  <th><strong>{{$data['nodeName']}}</strong><button type="button" class="btn btn-outline-success btn-icon-text" onclick="downloadFile('{{$data['nodeName']}}')">Exportar a CSV  <i class="mdi mdi-export btn-icon-prepend"></i></button></th>
                </tr>
              @foreach($data['data'] as $key => $nodes)
              <tr>
                <td>
                    @foreach($nodes as $key => $text)
                      <div class="box box-info">
                        <i>{{$key}}: </i><a>{{$text}}</a>
                      </div>
                    @endforeach
                  </td>
               </tr>     
              @endforeach
              </table> 
            </div>
        </div>
    </div>
    <br>
@endforeach     
</div>

<div id="miFrame" height="500" align="right" style="width:50%; float:right;">
  
</div>


@endsection
@section('scriptFooter')
<script>
  function arrayObjToCsv(ar, name) {
  //comprobamos compatibilidad
  if(window.Blob && (window.URL || window.webkitURL)){
    var contenido = "",
      d = new Date(),
      blob,
      reader,
      save,
      clicEvent;
    //creamos contenido del archivo
    for (var i = 0; i < ar.length; i++) {
      //construimos cabecera del csv
      //if (i == 0)
        //contenido += Object.keys(ar[i]).join(";") + "\n";
      //resto del contenido
      contenido += Object.keys(ar[i]).map(function(key){
              return ar[i][key];
            }).join(";") + "\n";
    }
    //creamos el blob
    blob =  new Blob(["\ufeff", contenido], {type: 'text/csv'});
    //creamos el reader
    var reader = new FileReader();
    reader.onload = function (event) {
      //escuchamos su evento load y creamos un enlace en dom
      save = document.createElement('a');
      save.href = event.target.result;
      save.target = '_blank';
      //aquí le damos nombre al archivo
      save.download = "{{$extractorName}}_"+"nodo_"+name+"_"+d.getDate() + "_" + (d.getMonth()+1) + "_" + d.getFullYear() +".csv";
      try {
        //creamos un evento click
        clicEvent = new MouseEvent('click', {
          'view': window,
          'bubbles': true,
          'cancelable': true
        });
      } catch (e) {
        //si llega aquí es que probablemente implemente la forma antigua de crear un enlace
        clicEvent = document.createEvent("MouseEvent");
        clicEvent.initEvent('click', true, true);
      }
      //disparamos el evento
      save.dispatchEvent(clicEvent);
      //liberamos el objeto window.URL
      (window.URL || window.webkitURL).revokeObjectURL(save.href);
    }
    //leemos como url
    reader.readAsDataURL(blob);
  }else {
    //el navegador no admite esta opción
    alert("Su navegador no permite esta acción");
  }
};

function downloadFile(name) {
  var extracted = <?php echo json_encode($extractedData);  ?>;
  //var newEscalafon = JSON.parse(escalafon);
  //console.log(extracted[0]['data']);
  var arrayToImport=[];
  extracted.forEach(function(element) {
    if(element['nodeName']==name){
      element['data'].forEach(function(element2) {
        arrayToImport.push(element2);
        console.log(arrayToImport);
       });
    }
    
  });

  arrayObjToCsv(arrayToImport, name);
}


        $(document).ready(function() {
          $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });

            (prepareFrame)();

          
           $('#exportCSV').on('click', function (e) {
            
            $.ajax({
                  url: '{{Route('exportCSV')}}',
                  type: 'POST',
                  dataType: 'JSON',
                  data: {Extracted},
                  beforeSend: function () {
                    swal({
                      title: 'Espere...',
                      text: 'Exportanto',
                      allowOutsideClick: false,
                      onOpen: () => {
                          swal.showLoading()
                        }
                      }).then((result) => {
                          if (result.dismiss === 'timer') {
                          }
                      })
                      },
                  success: function(result) {
                    swal({
                      type: 'success',
                      title: 'Exportado!',
                      text: 'Bien Hecho',
                      allowOutsideClick: false,
                    }).then(function () {
                          location.reload();
                      })
                  }
                  }).fail(function() {
                      swal(
                          '¡Error!',
                          'Algo a salido mal',
                          'error'
                      )
                  });
                         
           } );
        });

  function prepareFrame() {
    var ifrm = document.createElement("iframe");
    ifrm.setAttribute("src", "{{$url}}");
    ifrm.style.width = "100%";
    ifrm.style.height = "500px";
    ifrm.style.float = "right";
    $( "#miFrame" ).append(ifrm);
  }

</script>

@endsection