<!DOCTYPE html>
<html lang="es">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Text Extraction Community</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{ asset('new-style/vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('new-style/vendors/css/vendor.bundle.base.css') }}">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{ asset('new-style/css/style.css') }}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{ asset('new-style/images/somosmini.png') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('dist/css/sweetalert2.min.css') }}">
  
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
  
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth">
        <div class="row w-100">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left p-5">
              <div class="brand-logo">
                <img src="{{ asset('new-style/images/somos.png') }}">
              </div>
              <h4 class="text-danger">{{$error}}</h4>
              <h4>¡Hola! empecemos</h4>

              <h6 class="font-weight-light">Inicia sesión para continuar.</h6>
              <form class="pt-3" method="POST" action="{{ route('loginAuth') }}">
                <div class="form-group">
                  <input type="text" class="form-control form-control-lg" id="exampleInputEmail1" name="login" placeholder="Usuario" required>
                </div>
                <div class="form-group">
                  <input type="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Contraseña" name="password" required>
                </div>
                <div class="mt-3">
                  <button type="submit" class="btn btn-block btn-gradient-info btn-lg font-weight-medium auth-form-btn">ENTRAR</button>
                </div>
                <div class="text-center mt-4 font-weight-light">
                  <a id="forgot" href="#" class="auth-link text-black">¿Olvidaste tu contraseña?</a>
                </div>
                <div class="text-center mt-4 font-weight-light">
                  <a id="reset" href="#" class="auth-link">Tengo un código de recuperación</a>
                </div>
                <div class="text-center mt-4 font-weight-light">
                  ¿No tienes una cuenta? <a id="register" href=# class="text-info">Crear</a>
                </div>
                <div class="text-center mt-4 font-weight-light">
                  Activa tu cuenta <a id="active" href=# class="text-info">Activar</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>


  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="{{ asset('new-style/vendors/js/vendor.bundle.base.js') }}"></script>
  <script src="{{ asset('new-style/vendors/js/vendor.bundle.addons.js') }}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="{{ asset('new-style/js/off-canvas.js') }}"></script>
  <script src="{{ asset('new-style/js/misc.js') }}"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <!-- End custom js for this page-->
  <script type="text/javascript" src="{{ asset('dist/js/sweetalert2.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jQuery/jquery-3.1.1.min.js') }}"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>

    <script>
    var numbers="0123456789";

    function have_numbers(string){
       for(i=0; i<string.length; i++){
          if (numbers.indexOf(string.charAt(i),0)!=-1){
             return 1;
          }
       }
       return 0;
    } 

    var letters="abcdefghijklmnñopqrstuvwxyz";

    function have_letters(string){
      string = string.toLowerCase();
        for(i=0; i<string.length; i++){
           if (letters.indexOf(string.charAt(i),0)!=-1){
             return 1;
        }
      }
      return 0;
    } 

    $(document).ready(function() {

        //REGISTRO DE USUARIO
        $('#register').click( function (e) {
            swal({
                title: 'Registro Nuevo Usuario',
                html:
                `<div class="card">
      				    <div class="card-body">
      				      <form class="forms-sample">
      				        <div class="form-group">
      				          <label for="exampleInputUsername1">Nombre de usuario (solo letras y números)</label>
      				          <input type="text" class="form-control" name="nameUser" id="swal-input0" placeholder="Nombre Usuario">
      				        </div>
      				        <div class="form-group">
      				          <label for="exampleInputEmail1">Email</label>
      				          <input type="email" class="form-control" name="email" id="swal-input1" placeholder="Email">
      				        </div>
      				        <div class="form-group">
      				          <label for="exampleInputPassword1">Contraseña</label>
      				          <input type="password" class="form-control" name="newPassword" id="swal-input2" placeholder="Password">
      				        </div>
      				        <div class="form-group">
      				          <label for="exampleInputConfirmPassword1">Repita su contraseña</label>
      				          <input type="password" class="form-control" name="repeatPassword" id="swal-input3" placeholder="Password">
      				        </div>
      				      </form>
      				    </div>
      				  </div>`,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar',
                cancelButtonText: 'Cancelar',
                confirmButtonClass: 'btn btn-gradient-info btn-fw',
  				cancelButtonClass: 'btn btn-gradient-danger btn-fw',
                onOpen: function() {
                },
                preConfirm: function () {
                  return new Promise(function (resolve, reject) {
                    if ($('#swal-input2').val() != $('#swal-input3').val()){
                        reject('Las contraseñas ingresadas no coinciden.')
                    }

                    if ($('#swal-input0').val()==='' || $('#swal-input1').val()==='' || $('#swal-input2').val()==='' || $('#swal-input3').val()===''){
                      if ($('#swal-input0').val()===''){
                        reject('Debe ingresar un nombre de usuario.')
                      }

                      if ($('#swal-input1').val()===''){
                        reject('Debe ingresar un email.')
                      }
                      if ($('#swal-input2').val()===''){
                        reject('Debe ingresar una contraseña.')
                      }
                      if ($('#swal-input3').val()===''){
                        reject('Debe repetir su contraseña.')
                      }
                    }
                    else {
                      if ($('#swal-input0').val().length<8){
                        reject('El de nombre de usuario debe tener minimo 8 carácteres.')
                      }
                      if (!have_letters($('#swal-input0').val()) || !have_numbers($('#swal-input0').val())){
                          reject('Su nombre de usuario debe ser alfanumérico.')
                      }
                      if (!$('#swal-input1').val().includes('@') ){
                        reject('Debe ingresar un Email valido.')
                      }
                      if ($('#swal-input2').val().length<8){
                        reject('La contraseña debe tener minimo 8 carácteres.')
                      }
                      if ($('#swal-input2').val()!==$('#swal-input3').val()){
                        reject('Las contraseñas no coinciden.')
                      }
                      else{
                        resolve([
                          $('#swal-input0').val(),
                          $('#swal-input1').val(),
                          $('#swal-input2').val(),
                          $('#swal-input3').val(),
                        ])
                      }
                    }
                    
                  })
                }
              }).then((result) => {
                console.log(result);
                var url = '{{route('registerUser')}}';

                $.ajax({
                    url : url,
                    type : 'POST',
                    dataType: 'JSON',
                    data: { login:result[0], 
                            email:result[1], 
                            password:result[2],
                            passwordRepeat:result[3]
                            },
                  beforeSend: function () {
                    swal({
                      title: 'Espere...',
                      text: 'Creando nuevo usuario',
                      allowOutsideClick: false,
                      onOpen: () => {
                        swal.showLoading()
                      }
                    }).then((result) => {
                      if (result.dismiss === 'timer') {
                        console.log('I was closed by the timer')
                      }
                    })
                  },
                  success: function(result) {
                    console.log(result);
                    if(result.data.request=="SUCCESSFUL"){
                      swal({
                        type: 'success',
                        title: '¡Enviado!',
                        text: 'El nuevo usuario a sido creado con exito, se a enviado un correo con el código de activación para su cuenta.',
                        allowOutsideClick: false,
                      }).then(function () {
                      
                      })
                    }else{
                      swal({
                        type: 'error',
                        title: '¡Error!',
                        text: JSON.stringify(result.data.errorsContent),
                        allowOutsideClick: false,
                      }).then(function () {
                      
                      })
                    }
                    
                  }
                }).fail(function() {
                  swal(
                  '¡Error!',
                  'El registro no pudo ser realizado, favor intente mas tarde',
                  'error')
                });
              })
        });
        //FIN REGISTRO DE USUARIO

        //ACTIVACION DE CUENTA
        $('#active').click( function (e) {
            swal({
                title: 'Activar Cuenta',
                html:
                `<div class="card">
      				    <div class="card-body">
      				      <form class="forms-sample">
      				        <div class="form-group">
      				          <label for="exampleInputUsername1">Nombre de usuario</label>
      				          <input type="text" class="form-control" name="nameUser" id="swal-input0" placeholder="Nombre Usuario">
      				        </div>
      				        <div class="form-group">
      				          <label for="exampleInputEmail1">Email</label>
      				          <input type="email" class="form-control" name="email" id="swal-input1" placeholder="Email">
      				        </div>
      				        <div class="form-group">
      				          <label for="exampleInputCode">Código de activación</label>
      				          <input type="text" class="form-control" name="codeActive" id="swal-input2" placeholder="Password">
      				        </div>
      				      </form>
      				    </div>
      				  </div>`,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar',
                cancelButtonText: 'Cancelar',
                confirmButtonClass: 'btn btn-gradient-info btn-fw',
  				cancelButtonClass: 'btn btn-gradient-danger btn-fw',
                onOpen: function() {
                },
                preConfirm: function () {
                  return new Promise(function (resolve, reject) {

                    if ($('#swal-input0').val()==='' || $('#swal-input1').val()==='' || $('#swal-input2').val()===''){
                      if ($('#swal-input0').val()===''){
                        reject('Debe ingresar un nombre de usuario.')
                      }
                      if ($('#swal-input1').val()===''){
                        reject('Debe ingresar un email.')
                      }
                      if ($('#swal-input2').val()===''){
                        reject('Debe ingresar el coódigo de activación.')
                      }
                    }
                    else {
                      resolve([
                        $('#swal-input0').val(),
                        $('#swal-input1').val(),
                        $('#swal-input2').val(),
                      ])
                    }
                    
                  })
                }
              }).then((result) => {
                console.log(result);
                var url = '{{route('activation')}}';

                $.ajax({
                    url : url,
                    type : 'POST',
                    dataType: 'JSON',
                    data: { login:result[0], 
                            email:result[1], 
                            token:result[2],
                            },
                  beforeSend: function () {
                    swal({
                      title: 'Espere...',
                      text: 'Activando usuario',
                      allowOutsideClick: false,
                      onOpen: () => {
                        swal.showLoading()
                      }
                    }).then((result) => {
                      if (result.dismiss === 'timer') {
                        console.log('I was closed by the timer')
                      }
                    })
                  },
                  success: function(result) {
                    
                    if(result.data.request=="SUCCESSFUL"){
                      swal({
                        type: 'success',
                        title: '¡Enviado!',
                        text: 'El usuario a sido activado con exito.',
                        allowOutsideClick: false,
                      }).then(function () {
                        
                      })
                    }else{
                      swal({
                        type: 'error',
                        title: '¡Error!',
                        text: JSON.stringify(result.data.errorsContent),
                        allowOutsideClick: false,
                      }).then(function () {
                      
                      })
                    }
                  }
                }).fail(function() {
                  swal(
                  '¡Error!',
                  'La activacón no pudo ser realizada, favor intente mas tarde',
                  'error')
                });
              })
        });

//Olvidaste tu contraseña
        $('#forgot').on('click', function (e) {
            swal({
                title: 'Recupera tu contraseña',
                type: 'info',
                html:
                `<div class="card">
                     <div class="body">
                       
                              <div >
                                <div class="form-group" id='namediv'>
                                    <div class="form-line">
                                      <label>Ingresa el Email de tu cuenta</label>
                                        <input type="text" id="swal-input0" name="nameUser" class="form-control camel-case" required>
                                    </div>
                                  </div>
                              </div>
                        
                      </div>
                    </div>`,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar',
                cancelButtonText: 'Cancelar',
                onOpen: function() {
                },
                preConfirm: function () {
                  return new Promise(function (resolve, reject) {
                    if ($('#swal-input0').val()==='' ){
                        reject('Debe ingresar un Email.')
                    }
                    if (!$('#swal-input0').val().includes('@') ){
                        reject('Debe ingresar un Email valido.')
                    }
                    else {
                      resolve([
                        $('#swal-input0').val(),
                      ])
                    }
                    
                  })
                }
              }).then((result) => {
                console.log(result);
                var url = '{{route('forgot.password')}}';

                $.ajax({
                  headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
                    url : url,
                    type : 'POST',
                    dataType: 'JSON',
                    data: {email:result[0]},
                  beforeSend: function () {
                    swal({
                      title: 'Espere...',
                      text: 'Enviando solicitud',
                      allowOutsideClick: false,
                      onOpen: () => {
                        swal.showLoading()
                      }
                    }).then((result) => {
                      if (result.dismiss === 'timer') {
                        console.log('I was closed by the timer')
                      }
                    })
                  },
                  success: function(result) {
                    if (result.data.request=="SUCCESSFUL") {
                        swal({
                          type: 'success',
                          title: '¡Enviado!',
                          text: 'Se a enviado un correo con el código para realizar el proceso de reinicio de contraseña.',
                          allowOutsideClick: false,
                        }).then(function () {})

                    }else{
                      swal({
                        type: 'error',
                        title: '¡Error!',
                        text: JSON.stringify(result.data.errorsContent),
                        allowOutsideClick: false,
                      }).then(function () {
                      
                      })
                    }
                    
                  }
                }).fail(function() {
                  swal(
                  '¡Error!',
                  'El registro no pudo ser realizado, favor intente mas tarde',
                  'error')
                });
              })
        });
        //Fin olvidaste tu contraseña
        //RESET PASSWORD
        $('#reset').click( function (e) {
            swal({
                title: 'Activar Cuenta',
                html:
                `<div class="card">
                  <div class="card-body">
                    <form class="forms-sample">
                      <div class="form-group">
                        <label for="exampleInputUsername1">Nombre de usuario</label>
                        <input type="text" class="form-control" name="nameUser" id="swal-input0" placeholder="Nombre Usuario">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" class="form-control" name="email" id="swal-input1" placeholder="Email">
                      </div>
                       <div class="form-group">
                        <label for="exampleInputPassword1">Nueva Contraseña</label>
                        <input type="password" class="form-control" name="newPassword" id="swal-input2" placeholder="Password">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputConfirmPassword1">Repita su contraseña</label>
                        <input type="password" class="form-control" name="repeatPassword" id="swal-input3" placeholder="Password">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputCode">Código de recuperación</label>
                        <input type="text" class="form-control" name="codeActive" id="swal-input4" placeholder="Código">
                      </div>
                    </form>
                  </div>
                </div>`,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar',
                cancelButtonText: 'Cancelar',
                confirmButtonClass: 'btn btn-gradient-info btn-fw',
                cancelButtonClass: 'btn btn-gradient-danger btn-fw',
                onOpen: function() {
                },
                preConfirm: function () {
                  return new Promise(function (resolve, reject) {

                    if ($('#swal-input0').val()==='' || $('#swal-input1').val()==='' || $('#swal-input2').val()==='' || $('#swal-input3').val()==='' || $('#swal-input4').val()===''){
                      if ($('#swal-input0').val()===''){
                        reject('Debe ingresar un nombre de usuario.')
                      }
                      if ($('#swal-input1').val()===''){
                        reject('Debe ingresar un email.')
                      }
                      if ($('#swal-input2').val()===''){
                        reject('Debe ingresar su nueva contraseña.')
                      }
                      if ($('#swal-input3').val()===''){
                        reject('Debe repetir su contraseña.')
                      }
                      if ($('#swal-input4').val()===''){
                        reject('Debe ingresar el código de recuperación.')
                      }
                    }
                    else {
                      if ($('#swal-input0').val().length<8){
                        reject('El de nombre de usuario debe tener minimo 8 carácteres.')
                      }
                      if (!have_letters($('#swal-input0').val()) || !have_numbers($('#swal-input0').val())){
                          reject('Su nombre de usuario debe ser alfanumérico.')
                      }
                      if (!$('#swal-input1').val().includes('@') ){
                        reject('Debe ingresar un Email valido.')
                      }
                      if ($('#swal-input2').val().length<8){
                        reject('La contraseña debe tener minimo 8 carácteres.')
                      }
                      if ($('#swal-input2').val()!==$('#swal-input3').val()){
                        reject('Las contraseñas no coinciden.')
                      }else{
                        resolve([
                          $('#swal-input0').val(),
                          $('#swal-input1').val(),
                          $('#swal-input2').val(),
                          $('#swal-input3').val(),
                          $('#swal-input4').val(),
                        ])
                      }
                      
                    }
                    
                  })
                }
              }).then((result) => {
                console.log(result);
                var url = '{{route('reset.password')}}';

                $.ajax({
                    url : url,
                    type : 'POST',
                    dataType: 'JSON',
                    data: { login:result[0], 
                            email:result[1], 
                            newPassword:result[2],
                            newPasswordRepeat:result[3], 
                            token:result[4],
                            },
                  beforeSend: function () {
                    swal({
                      title: 'Espere...',
                      text: 'Enviando solicitud',
                      allowOutsideClick: false,
                      onOpen: () => {
                        swal.showLoading()
                      }
                    }).then((result) => {
                      if (result.dismiss === 'timer') {
                        console.log('I was closed by the timer')
                      }
                    })
                  },
                  success: function(result) {
                    if (result.data.request=="SUCCESSFUL") {
                        swal({
                          type: 'success',
                          title: '¡Enviado!',
                          text: 'Contaseña reinciada con exito.',
                          allowOutsideClick: false,
                        }).then(function () {
                          
                        })

                    }else{
                      swal({
                        type: 'error',
                        title: '¡Error!',
                        text: JSON.stringify(result.data.errorsContent),
                        allowOutsideClick: false,
                      }).then(function () {
                      
                      })
                    }
                  }
                }).fail(function() {
                  swal(
                  '¡Error!',
                  'El reinicio de contraseña no pudo ser realizada, favor intente mas tarde',
                  'error')
                });
              })
        });


    });
</script>
<!--token de autorización para envio de peticiones-->
<input type="hidden" name="_token" value="{{ csrf_token() }}" />
</body>

</html>
