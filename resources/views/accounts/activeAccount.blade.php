@extends('layout.newMain')
@section('content')
 <style type="text/css">
    .card .card-body {
        padding: 2.5rem 2.5rem;
        background-color: gainsboro !important;
    }
  </style>
<div class="page-header">
    <h3 class="page-title">
      <span>
                       
      </span>
      Debes Activar tu cuenta
    </h3>
</div>




              <div class="card">
                <div class="card-body">
                 <button type="button" class="btn btn-success btn-fw test" id="active">Activar Cuenta</button>
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom"></div>
                 <button type="button" class="btn btn-success btn-fw test" id="changeEmail">Cambiar Correo</button>
                
              </div>
              <!--button onclick="newExtract()" class="btn btn-info">ver array</button-->
            </div>
         


@endsection
@section('scriptFooter')
<script>
	var user = {!! json_encode($user) !!};
	//ACTIVACION DE CUENTA
        $('#active').click( function (e) {
            swal({
                title: 'Activar Cuenta',
                html:
                `<div class="card">
      				    <div class="card-body">
      				      <form class="forms-sample">
      				        <div class="form-group">
      				          <label for="exampleInputUsername1">Nombre de usuario</label>
      				          <input type="text" value="`+user.login+`" class="form-control" name="nameUser" id="swal-input0" placeholder="Nombre Usuario">
      				        </div>
      				        <div class="form-group">
      				          <label for="exampleInputEmail1">Email</label>
      				          <input type="email" value="`+user.email+`" class="form-control" name="email" id="swal-input1" placeholder="Email">
      				        </div>
      				        <div class="form-group">
      				          <label for="exampleInputCode">Código de activación</label>
      				          <input type="text" class="form-control" name="codeActive" id="swal-input2" placeholder="Código">
      				        </div>
      				      </form>
      				    </div>
      				  </div>`,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar',
                cancelButtonText: 'Cancelar',
                confirmButtonClass: 'btn btn-gradient-info btn-fw',
  				cancelButtonClass: 'btn btn-gradient-danger btn-fw',
                onOpen: function() {
                },
                preConfirm: function () {
                  return new Promise(function (resolve, reject) {

                    if ($('#swal-input0').val()==='' || $('#swal-input1').val()==='' || $('#swal-input2').val()===''){
                      if ($('#swal-input0').val()===''){
                        reject('Debe ingresar un nombre de usuario.')
                      }
                      if ($('#swal-input1').val()===''){
                        reject('Debe ingresar un email.')
                      }
                      if ($('#swal-input2').val()===''){
                        reject('Debe ingresar el coódigo de activación.')
                      }
                    }
                    else {
                      resolve([
                        $('#swal-input0').val(),
                        $('#swal-input1').val(),
                        $('#swal-input2').val(),
                      ])
                    }
                    
                  })
                }
              }).then((result) => {
                console.log(result);
                var url = '{{route('activation')}}';

                $.ajax({
                    url : url,
                    type : 'POST',
                    dataType: 'JSON',
                    data: { login:result[0], 
                            email:result[1], 
                            token:result[2],
                            },
                  beforeSend: function () {
                    swal({
                      title: 'Espere...',
                      text: 'Activando usuario',
                      allowOutsideClick: false,
                      onOpen: () => {
                        swal.showLoading()
                      }
                    }).then((result) => {
                      if (result.dismiss === 'timer') {
                        console.log('I was closed by the timer')
                      }
                    })
                  },
                  success: function(result) {
                    
                    if(result.data.request=="SUCCESSFUL"){
                      swal({
                        type: 'success',
                        title: '¡Enviado!',
                        text: 'El usuario a sido activado con exito.',
                        allowOutsideClick: false,
                      }).then(function () {
                        location.href ="{{Route('logout')}}";
                      })
                    }else{
                      swal({
                        type: 'error',
                        title: '¡Error!',
                        text: JSON.stringify(result.data.errorsContent),
                        allowOutsideClick: false,
                      }).then(function () {
                      
                      })
                    }
                  }
                }).fail(function() {
                  swal(
                  '¡Error!',
                  'La activacón no pudo ser realizada, favor intente mas tarde',
                  'error')
                });
              })
        });

		 $('#changeEmail').click( function (e) {
		 swal({
                title: 'Cambio de Email',
                html:
                `<div class="card">
      				    <div class="card-body">
      				      <form class="forms-sample">
      				        <div class="form-group">
      				          <label for="email">Nuevo Email</label>
      				          <input type="email" class="form-control" name="email" id="swal-input0" placeholder="Nuevo Email">
      				        </div>
      				        <div class="form-group">
      				          <label for="password">Su Contraseña</label>
      				          <input type="password" class="form-control" name="password" id="swal-input1" placeholder="Contraseña">
      				        </div>
      				      </form>
      				    </div>
      				  </div>`,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar',
                cancelButtonText: 'Cancelar',
                confirmButtonClass: 'btn btn-gradient-info btn-fw',
  				cancelButtonClass: 'btn btn-gradient-danger btn-fw',
                onOpen: function() {
                },
                preConfirm: function () {
                  return new Promise(function (resolve, reject) {

                    if ($('#swal-input0').val()==='' || $('#swal-input1').val()===''){
                      if ($('#swal-input0').val()===''){
                        reject('Debe ingresar un email.')
                      }
                      if ($('#swal-input1').val()===''){
                        reject('Debe ingresar su contraseña.')
                      }
                      
                    }
                    else {
                      resolve([
                        $('#swal-input0').val(),
                        $('#swal-input1').val(),
                      ])
                    }
                    
                  })
                }
              }).then((result) => {
                console.log(result);
                var url = '{{route('chancheEmail')}}';

                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
                    url : url,
                    type : 'get',
                    dataType: 'JSON',
                    data: {  login : "",
                    	       email : "",
                							password : result[1],
                							newEmail : result[0],
                            },
                  beforeSend: function () {
                    swal({
                      title: 'Espere...',
                      text: 'Activando usuario',
                      allowOutsideClick: false,
                      onOpen: () => {
                        swal.showLoading()
                      }
                    }).then((result) => {
                      if (result.dismiss === 'timer') {
                        console.log('I was closed by the timer')
                      }
                    })
                  },
                  success: function(result) {
                    console.log(result);
                    swal({
                      type: 'success',
                      title: '¡Enviado!',
                      text: 'El usuario a sido activado con exito.',
                      allowOutsideClick: false,
                    }).then(function () {
                       location.href ="{{Route('logout')}}";
                    })
                  }
                }).fail(function() {
                  swal(
                  '¡Error!',
                  'La activacón no pudo ser realizada, favor intente mas tarde',
                  'error')
                });
              });

	});
    
</script>
@endsection