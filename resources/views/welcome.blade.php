<!DOCTYPE html>
<html lang="es">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <title>Text Extraction Community</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="{{ asset('new-style/vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('new-style/vendors/css/vendor.bundle.base.css') }}">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{ asset('new-style/css/style.css') }}">
  <!-- endinject -->
  <link rel="shortcut icon" href="{{ asset('new-style/images/somosmini.png') }}">
  <!-- Personal -->
  <link rel="stylesheet" type="text/css" href="{{ asset('dist/css/sweetalert2.min.css') }}">
  
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
  

  <style type="text/css">
    .hidden {
      display: none !important;
    }
  </style>
</head>

<body>
  <div class="container-scroller">
    <!-- partial:../../partials/_navbar.html -->
    <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo" href="{{route('start.home')}}"><img src="{{ asset('new-style/images/somos.png') }}" alt="logo"/></a>
        <a class="navbar-brand brand-logo-mini"><img src="{{ asset('new-style/images/somosmini.png') }}" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-stretch">
        <ul class="navbar-nav navbar-nav-right">
          <button type="button" onclick="location.href='{{route('login')}}'" class="btn btn-outline-info btn-fw">Ingresar</button>
        </ul>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      
      <!-- partial -->
      <div style="width: 100%" class="main-panel">
        <div class="content-wrapper">
         <h1 class="display-1 mb-0 text-info" style="text-align: center;">Crea y utiliza extractores de texto para diferentes sitios Web</h1>
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom"></div>
            <div class="col text-center">
             <button type="button" class="btn btn-outline-danger btn-icon-text" onclick="location.href='{{route('extractors.extractorsPublic')}}'">
                      <i class="mdi mdi-download btn-icon-prepend"></i>                                                    
                      Ir a probar Extractores!
                    </button>
          </div>
        </div>
        
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->
        <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2019 <a href="https://dsi.face.ubiobio.cl/somos/" target="_blank">SOMOS U.B.B</a>. Todos los derechos reservados.</span>
            <!--span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hecho Con <i class="mdi mdi-heart text-danger"></i></span-->
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="{{ asset('new-style/vendors/js/vendor.bundle.base.js') }}"></script>
  <script src="{{ asset('new-style/vendors/js/vendor.bundle.addons.js') }}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="{{ asset('new-style/js/off-canvas.js') }}"></script>
  <script src="{{ asset('new-style/js/misc.js') }}"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <!-- End custom js for this page-->
  <script type="text/javascript" src="{{ asset('dist/js/sweetalert2.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jQuery/jquery-3.1.1.min.js') }}"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>
</body>

</html>
