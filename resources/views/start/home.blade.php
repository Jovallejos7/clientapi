@extends('layout.newMain')
@section('content')
<div class="page-header">
	<h3 class="page-title">
	  <span class="page-title-icon bg-gradient-info text-white mr-2">
	    <i class="mdi mdi-home"></i>                 
	  </span>
	  Inicio
	</h3>
</div>
<div class="row">
	<div class="col-md-4 stretch-card grid-margin">
	  <button class="card bg-gradient-danger card-img-holder text-white" onclick="location.href='{{route('extractors.myextractors')}}'">
	    <div class="card-body">
	      <img src="{{ asset('new-style/images/dashboard/circle.svg') }}" class="card-img-absolute" alt="circle-image">
	      <h4 class="font-weight-normal mb-3">Mis Extractores
	        <i class="mdi mdi-clipboard-outline mdi-24px float-right"></i>
	      </h4>
	      <h2 class="mb-5">Crea y utiliza tus propios extractores.</h2>
	      <i class="mdi mdi-exit-to-app" style="font-size: xx-large;"></i>
	    </div>
	  </button>
	</div>
	<div class="col-md-4 stretch-card grid-margin">
	  <button class="card bg-gradient-success card-img-holder text-white" onclick="location.href='{{route('extractors.extractors')}}'">
	    <div class="card-body">
	      <img src="{{ asset('new-style/images/dashboard/circle.svg') }}" class="card-img-absolute" alt="circle-image">                  
	      <h4 class="font-weight-normal mb-3">Extractores
	        <i class="mdi mdi-clipboard-text mdi-24px float-right"></i>
	      </h4>
	      <h2 class="mb-5">Accede a extractores ya creados.</h2>
	      <i class="mdi mdi-exit-to-app" style="font-size: xx-large;"></i>
	    </div>
	  </button>
	</div>
	<div class="col-md-4 stretch-card grid-margin">
	  <button class="card bg-gradient-info card-img-holder text-white" onclick="location.href='{{route('extractors.twitter')}}'">
	    <div class="card-body">
	      <img src="{{ asset('new-style/images/dashboard/circle.svg') }}" class="card-img-absolute" alt="circle-image">                                    
	      <h4 class="font-weight-normal mb-3">Twitter
	        <i class="mdi mdi-twitter mdi-24px float-right"></i>
	      </h4>
	      <h2 class="mb-5">Extrae Contenido desde Twitter.</h2>
	      	          <i class="mdi mdi-exit-to-app" style="font-size: xx-large;"></i>
	      
	    </div>
	  </button>
	</div>
</div>


@endsection