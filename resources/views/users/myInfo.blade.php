@extends('layout.newMain')
@section('content')
<div class="page-header">
  <h3 class="page-title">
    <span class="page-title-icon bg-gradient-info text-white mr-2">
      <i class="mdi mdi-clipboard-text"></i>                 
    </span>
    Mi Informacion
  </h3>
</div>

<div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <table class="table table-striped">
                      <tr>
                        <td>
                          Mi ID:
                        </td>
                        <td>
                          {{$info['id']}}
                        </td>
                        <td>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Nombre de Usuario:
                        </td>
                        <td>
                          {{$info['login']}}
                        </td>
                         <td>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Email:
                        </td>
                        <td>
                          {{$info['email']}}   
                        </td>
                        <td>
                          <button type="button" onclick="changeEmail();"  class="btn btn-gradient-info btn-icon-text">
                          <i class="mdi mdi-table-edit btn-icon-prepend"></i>
                          Modificar
                        </button>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Fecha de registro:
                        </td>
                        <td>
                          {{$info['registrationDate']}}
                        </td>
                         <td>
                        </td>
                      </tr>
                      
                  </table>
                </div>
              </div>
            </div>


@endsection
@section('scriptFooter')
<script>
	function changeEmail(){
		 swal({
                title: 'Cambio de Email',
                html:
                `<div class="card">
      				    <div class="card-body">
      				      <form class="forms-sample">
      				        <div class="form-group">
      				          <label for="email">Nuevo Email</label>
      				          <input type="email" class="form-control" name="email" id="swal-input0" placeholder="Nuevo Email">
      				        </div>
      				        <div class="form-group">
      				          <label for="password">Su Contraseña</label>
      				          <input type="password" class="form-control" name="password" id="swal-input1" placeholder="Contraseña">
      				        </div>
      				      </form>
      				    </div>
      				  </div>`,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirmar',
                cancelButtonText: 'Cancelar',
                confirmButtonClass: 'btn btn-gradient-info btn-fw',
  				cancelButtonClass: 'btn btn-gradient-danger btn-fw',
                onOpen: function() {
                },
                preConfirm: function () {
                  return new Promise(function (resolve, reject) {

                    if ($('#swal-input0').val()==='' || $('#swal-input1').val()===''){
                      if ($('#swal-input0').val()===''){
                        reject('Debe ingresar un email.')
                      }
                      if ($('#swal-input1').val()===''){
                        reject('Debe ingresar su contraseña.')
                      }
                      
                    }
                    else {
                      resolve([
                        $('#swal-input0').val(),
                        $('#swal-input1').val(),
                      ])
                    }
                    
                  })
                }
              }).then((result) => {
                console.log(result);
                var url = '{{route('chancheEmail')}}';

                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
                    url : url,
                    type : 'get',
                    dataType: 'JSON',
                    data: {  login : "",
                    	       email : "",
                							password : result[1],
                							newEmail : result[0],
                            },
                  beforeSend: function () {
                    swal({
                      title: 'Espere...',
                      text: 'Activando usuario',
                      allowOutsideClick: false,
                      onOpen: () => {
                        swal.showLoading()
                      }
                    }).then((result) => {
                      if (result.dismiss === 'timer') {
                        console.log('I was closed by the timer')
                      }
                    })
                  },
                  success: function(result) {
                    console.log(result);
                    swal({
                      type: 'success',
                      title: '¡Enviado!',
                      text: 'El usuario a sido activado con exito.',
                      allowOutsideClick: false,
                    }).then(function () {
                      
                    })
                  }
                }).fail(function() {
                  swal(
                  '¡Error!',
                  'La activacón no pudo ser realizada, favor intente mas tarde',
                  'error')
                });
              });

	}
        $(document).ready(function() {
            
        });
</script>
@endsection