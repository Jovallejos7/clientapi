@extends('layout.newMain')
@section('content')
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center text-center error-page bg-info">
        <div class="row flex-grow">
          <div class="col-lg-7 mx-auto text-white">
            <div class="row align-items-center d-flex flex-row">
              <div class="col-lg-6 text-lg-right pr-lg-4">
                <h2 class="display-1 mb-0">{{$code}} </h2>
              </div>
              <div class="col-lg-6 error-page-divider text-lg-left pl-lg-4">
                <h3>¡LO SENTIMOS!</h3>
                <h4 class="font-weight-light"> {{$error}}</h4>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
@endsection
